# Team 15 Project

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
        <li><a href="#website">Website</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

Our task was to develop DeliverIT - a web application that serves the needs of a freight forwarding company.
DeliverIT's customers can place orders on international shopping sites (like Amazon.de or eBay.com) and have their parcels delivered either to the company's warehouses or their own address.
DeliverIT has two types of users - customers and employees. The customers can see how many parcels they have on the way. The employees have a bit more capabilities. They can add new parcels to the system, modify existing ones, check how many parcels has a given warehouse received and more.

### Built With

Frameworks and add-ons/plugins.

- [MATERIAL UI](https://material-ui.com/)
- [Moment](https://momentjs.com/)
- [REST](https://restfulapi.net/)
- [Swagger](https://swagger.io/)
- [React](https://reactjs.org/)
- [ExpressJS](https://expressjs.com/)
- [NodeJS](https://nodejs.org/en/)

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple example steps.

## Website

You can click [HERE](https://deliver-it-15.web.app/) to visit it!

EXAMPLE EMPLOYEE ACCOUNT\
email : asen@mail.com\
password : team15team15

EXAMPLE CUSTOMER ACCOUNT\
email : denis@mail.com\
password : team15team15

or you can make your own ( only customer ! )

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/theharp3/team-15-project
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Open client and run
   ```sh
   npm start
   ```
4. Because it is hosted on heroku (back-end) and on clever-cloud (DB) you can not make changes to Back-end nor DB
5. You can make connection to the DB with the new connection in Workbench with .env props.

<!-- USAGE EXAMPLES -->

## Usage

You can register/log in and many more check our documentation

_For more examples, please refer to the [Documentation](https://app.swaggerhub.com/apis-docs/azsamdenis/Users-DeliverIT/0.1)_

<!-- ROADMAP -->

## Roadmap

See the [open issues](https://gitlab.com/theharp3/team-15-project/-/boards) for a list of proposed features (and known issues).

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- CONTACT -->

## Contact

Denislav Mihalev - [Facebook](https://www.facebook.com/profile.php?id=100005963809223) - mihalev.denislav@gmail.com

Asen Zhivkov -
asen.nikolaev.zhivkov@gmail.com
Project Link: [https://gitlab.com/theharp3/team-15-project](https://gitlab.com/theharp3/team-15-project)
