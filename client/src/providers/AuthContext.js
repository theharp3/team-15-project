import { createContext } from 'react';

const AuthContext = createContext({
  user: null,
  setUserContext: () => {},
});

export default AuthContext;
