import { Route, Redirect } from 'react-router';

const EmployeeRoute = ({ component: Component, auth, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        auth?.role === 'Employee' ? (
          <Component {...props} />
        ) : (
          <Redirect to="/" />
        )
      }
    />
  );
};

export default EmployeeRoute;
