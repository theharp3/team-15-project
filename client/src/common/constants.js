import { decodeToken } from '../utils/token';

export const BASE_URL = 'https://deliver-it-15.herokuapp.com/';

export const USER_PAYLOAD = decodeToken(localStorage.getItem('token')) || decodeToken(sessionStorage.getItem('token'));

export const SHIPMENTS_STATUS = {
  preparing: 1,
  enRoute: 2,
  delivered: 3,
};

export const SHIPMENTS_CARGO = {
  minWeight: 150,
  maxWeight: 250,
};
