import Container from '@material-ui/core/Container';
import React, { useEffect } from 'react';
import { useContext, useState } from 'react';
import AuthContext from '../../providers/AuthContext.js';
import CssBaseline from '@material-ui/core/CssBaseline';
import './UserProfile.css';
import { makeStyles, styled } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Dialog, DialogActions, DialogContentText, DialogTitle, InputAdornment, Paper, TextField, withStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { checkPass, updateUser } from '../../requests/user.js';
import { decodeToken } from '../../utils/token.js';
import CreateIcon from '@material-ui/icons/Create';
import IconButton from '@material-ui/core/IconButton';
import { DialogContent } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import Snackbars from '../Alerts/SnackBars.jsx';
import { purple } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  form: {
    width: '100%',
  },
  submit: {
    float: 'right',
    marginTop: '40px',
    marginLeft:'5px'
  },
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  textField:{
    fontColor:'white'
  },
  input: {
    color: 'white',
  },
  cssLabel: {
    color : 'white !important'
  }, 
  notchedOutline: {
    borderWidth: '1px',
    borderColor: 'white !important',
    color: 'white',
  },

}));
const ColorButton = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText(purple[500]),
    backgroundColor: purple[500],
    '&:hover': {
      backgroundColor: purple[700],
    },
  },
}))(Button);
export default function UserProfile() {
  const classes = useStyles();
  const { user, setUserContext } = useContext(AuthContext);
  const [userChange, setUserChange] = useState({});
  const [passChange, setPassChange] = useState({});

  const [update, setUpdate] = useState(false);
  const [open, setOpen] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showPassword1, setShowPassword1] = useState(false);
  const [repeatError, setRepeatError] = useState(false);
  const [wrongPassError, setWrongPassError] = useState(false);
  
  const [successChange, setSuccessChange] = useState(null);
  const [successEdit, setSuccessEdit] = useState(null);

  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleClickShowPassword1 = () => setShowPassword1(!showPassword1);

  const createUserEntry = (prop, value) => {
    userChange[`${prop}`] = value;
    setUserChange(userChange);
  };
  useEffect(() => {
    if (Object.keys(userChange).length !== 0) {
      updateUser(userChange, user.id).then((data) => {
        localStorage.removeItem('token');
        setUserContext(decodeToken(data.token));
        localStorage.setItem('token', data.token);
        setSuccessEdit(true);
      });
    }
    setUpdate(false);
  }, [update]);

  const changePass =(prop, value) =>{
    passChange[`${prop}`] = value;
    setPassChange(passChange);
  }

  const handleClickOpen = () => {
    setShowPassword(false);
    setShowPassword1(false);
    setRepeatError(false);
    setWrongPassError(false);
    setOpen(true);
  };
  const handleSubmit = () =>{
    setRepeatError(false);
    setWrongPassError(false);

    if(passChange.new_password !== passChange.repeat_password){
      setRepeatError(true);
    } else{
    checkPass(user.email, passChange.old_password).then((res)=>{
      if(res.message === 'Success'){
        updateUser({password:`${passChange.new_password}`},user.id).then((res)=>{
          if(res.token){
            setOpen(false);
            setSuccessChange(true);
          }
        })
      } else{
        setWrongPassError(true);
      }
    })
  }
  }
  const handleClose = () => {
    setOpen(false);
  };

  const createUserBtn = (e) => {
    e.preventDefault();
    setUpdate(true);
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column',color:'white',height:'calc(100vh - 104px)' }}>
    {successEdit && (
        <Snackbars
          show={true}
          setToNull={setSuccessEdit}
          severity={'success'}
          text={`Successfully edited your profile`}
        />
      )}
    {successChange && (
      <Snackbars
         show={true}
         setToNull={setSuccessChange}
         severity={'success'}
         text={`Successfully changed your password`}
       />
      )}
    <Paper style={{width:'100%',height:'100px',background:'linear-gradient( rgb(87, 48, 12),rgb(231, 130, 35)) center center / cover'}} >
    <CreateIcon style={{height:'50px',width:'50px',marginTop:'20px', marginLeft:'40px',color:'rgb(87, 48, 12)'}} />
    </Paper>
      <Container component="main" maxWidth="lg">
        <h2 style={{ marginTop: '60px', marginBottom: '30px' }}>
          Edit my profile
          <hr className="hr-2" />
        </h2>
      </Container>
      <Container component="main" maxWidth="lg">
        <CssBaseline>
          <div className={classes.root}>
            <form className={classes.form} noValidate onSubmit={createUserBtn}>
              <Grid container spacing={8}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    className={classes.textField}
                    autoComplete="off"
                    name="firstName"
                    variant="outlined"
                    fullWidth
                    id="firstName"
                    label="First Name"
                    defaultValue={user.first_name}
                    InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                    InputProps={{
                        classes: {
                          root: classes.notchedOutline,
                          focused: classes.notchedOutline,
                          notchedOutline: classes.notchedOutline,
                        },
                        
                    }}
                    onChange={(e) =>
                      createUserEntry('first_name', e.target.value)
                    }
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="off"
                    name="lastName"
                    variant="outlined"
                    fullWidth
                    id="lastName"
                    label="Last Name"
                    defaultValue={user.last_name}
                    InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                    InputProps={{
                        classes: {
                          root: classes.notchedOutline,
                          focused: classes.notchedOutline,
                          notchedOutline: classes.notchedOutline,
                        },
                        
                    }}
                    onChange={(e) =>
                      createUserEntry('last_name', e.target.value)
                    }
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="email"
                    name="email"
                    disabled
                    variant="outlined"
                    fullWidth
                    id="email"
                    label="Email"
                    defaultValue={user.email}
                    InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                    InputProps={{
                        classes: {
                          root: classes.notchedOutline,
                          focused: classes.notchedOutline,
                          notchedOutline: classes.notchedOutline,
                        },
                        
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="password"
                    name="password"
                    variant="outlined"
                    fullWidth
                    disabled
                    id="password"
                    label="Password"
                    defaultValue='*********'
                    InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                    InputProps={{
                        classes: {
                          root: classes.notchedOutline,
                          focused: classes.notchedOutline,
                          notchedOutline: classes.notchedOutline,
                        },
                        
                    }}
                  />
                </Grid>
                <Grid item xs={6} sm={3}>
                  <TextField
                    autoComplete="off"
                    name="country"
                    variant="outlined"
                    fullWidth
                    id="country"
                    label="Country"
                    defaultValue={user.user_country}
                    onChange={(e) =>
                      createUserEntry('user_country', e.target.value)
                    }
                    InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                    InputProps={{
                        classes: {
                          root: classes.notchedOutline,
                          focused: classes.notchedOutline,
                          notchedOutline: classes.notchedOutline,
                        },
                        
                    }}
                  />
                </Grid>
                <Grid item xs={6} sm={3}>
                  <TextField
                    autoComplete="off"
                    name="city"
                    variant="outlined"
                    fullWidth
                    id="city"
                    label="City"
                    defaultValue={user.user_city}
                    onChange={(e) =>
                      createUserEntry('user_city', e.target.value)
                    }
                    InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                    InputProps={{
                        classes: {
                          root: classes.notchedOutline,
                          focused: classes.notchedOutline,
                          notchedOutline: classes.notchedOutline,
                        },
                        
                    }}
                  />
                </Grid>
                <Grid item xs={6} sm={3}>
                  <TextField
                    autoComplete="off"
                    name="streetName"
                    variant="outlined"
                    fullWidth
                    id="streetName"
                    label="Street Name"
                    defaultValue={user.address}
                    onChange={(e) => createUserEntry('address', e.target.value)}
                    InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                    InputProps={{
                        classes: {
                          root: classes.notchedOutline,
                          focused: classes.notchedOutline,
                          notchedOutline: classes.notchedOutline,
                        },
                        
                    }}
                  />
                </Grid>
                <Grid item xs={6} sm={3}>
                  <TextField
                    autoComplete="postCode"
                    name="postCode"
                    variant="outlined"
                    fullWidth
                    id="postCode"
                    label="Post Code"
                    defaultValue={user.post_code}
                    onChange={(e) =>
                      createUserEntry('post_code', e.target.value)
                    }
                    InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                    InputProps={{
                        classes: {
                          root: classes.notchedOutline,
                          focused: classes.notchedOutline,
                          notchedOutline: classes.notchedOutline,
                        },
                        
                    }}
                  />
                </Grid>
              </Grid>
              <Button
                type="submit"
                color="secondary"
                className={classes.submit}
                onSubmit={createUserBtn}
                variant="contained"
              >
                Save changes
              </Button>
              <ColorButton  variant="contained" color="primary" className={classes.submit} onClick={handleClickOpen}>
                Change Password
              </ColorButton>
              <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Change Password</DialogTitle>
                <DialogContent>
                  <DialogContentText>
                    To change your password, please enter your old one here.
                  </DialogContentText>
                  <TextField
                    autoFocus
                    margin="dense"
                    id="passwordOld"
                    label="Current Password"
                    type={showPassword ? "text" : "password"}
                    fullWidth
                    error={wrongPassError}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                          >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                    helperText={
                      wrongPassError === false ? '' : 'Wrong Password'
                    }
                    onChange={(e) =>
                      changePass('old_password', e.target.value)
                    }
                  />
                  <TextField
                    autoFocus
                    margin="dense"
                    id="passwordNew"
                    label="New Password"
                    type={showPassword1 ? "text" : "password"}
                    fullWidth
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword1}
                          >
                            {showPassword1 ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                    onChange={(e) =>
                      changePass('new_password', e.target.value)
                    }
                  />
                 <TextField
                    autoFocus
                    margin="dense"
                    id="passwordNew"
                    label="Repeat New Password"
                    type="password"
                    fullWidth
                    error={repeatError}
                    onChange={(e) =>
                      changePass('repeat_password', e.target.value)
                    }
                    helperText={
                      repeatError === false ? '' : 'Passwords Do Not Match'
                  }
                  />
                </DialogContent>
                <DialogActions>
                  <Button onClick={handleClose} color="primary">
                    Cancel
                  </Button>
                  <Button onClick={handleSubmit} color="primary">
                    Submit
                  </Button>
                </DialogActions>
              </Dialog>
            </form>
          </div>
        </CssBaseline>
      </Container>
    </div>
  );
}
