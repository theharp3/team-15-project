import { useContext, useEffect, useState } from 'react';
import AuthContext from '../../providers/AuthContext.js';
import { useHistory } from 'react-router';
import './Header.css';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { alpha, Avatar, Divider, InputBase } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { Autocomplete } from '@material-ui/lab';
import { getMyParcels } from '../../requests/parcels.js';

const useStyles = makeStyles((theme) => ({
  appBar:{
    display: 'flex',
    justifyContent: 'space-between',
    background: 'linear-gradient(rgb(42, 112, 160), rgb(16, 45, 66)) center center / cover',
  },
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    fontFamily: 'inherit',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '300px',
      '&:focus': {
        width: '350px',
      },
    },
  },
  menuOpt:{
    display:'flex',
    alignItems: 'center',
  },
}));

function randomColor(letter) {
  let hex = Math.floor((letter.charCodeAt(0) / 110) * 0xffffff);
  let color = '#' + hex.toString(16);
  return color;
}

export default function Header() {
  const classes = useStyles();
  let history = useHistory();
  const { user, setUserContext } = useContext(AuthContext);
  const [parcels, setParcels] = useState([]);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  useEffect(()=>{
    if(user){
    getMyParcels(user.id).then((res)=>setParcels(res))}
  },[])
  const handleClose = () => {
    setAnchorEl(null);
  };
  const logOutBtn = () => {
    localStorage.clear();
    sessionStorage.clear();
    setUserContext(null);
    setAnchorEl(null);
    history.push('/');
  };
  const profileViewBtn = () => {
    setAnchorEl(null);
    history.push('/user');
  };
  const parcelsViewBtn = () => {
    setAnchorEl(null);
    history.push('/myparcels');
  };
  const goToParcel = (value) => {
    if(value !== null){
    const { id } = value;
    history.push(`/parcels/${id}`);
    }
  };


  return (
    <AppBar
      position="static"
      className={classes.appBar}
    >
      <Toolbar style={{ justifyContent: 'space-between' }}>
        <div style={{ display: 'flex', alignItems: 'center', width:'14%',justifyContent:'space-between' }}>
          <Typography variant="h6" style={{fontFamily:'Aclonica, sans-serif',fontSize:'30px'}}>
            PACE
          </Typography>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            href="/"
          >
            <Typography variant="h6" className={classes.title}>
              Home
            </Typography>
          </IconButton>
        </div>
        {user?.role === 'Employee' && (
          <div>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
              href="/employee/warehouses"
            >
              <Typography variant="h6" className={classes.title}>
                Warehouses
              </Typography>
            </IconButton>

            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
              href="/employee/shipments"
            >
              <Typography variant="h6" className={classes.title}>
                Shipments
              </Typography>
            </IconButton>

            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
              href="/employee/parcels"
            >
              <Typography variant="h6" className={classes.title}>
                Parcels
              </Typography>
            </IconButton>
          </div>
        )}
        <div className={classes.menuOpt}>
        {user?.role === 'Customer' ?
        (<div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <Autocomplete
          id="combo-box-demo"
          options={parcels}
          getOptionLabel={(option) => `Parcel ID #${120 + +option.id}`}
          style={{ width: 300 }}s
          onChange={(event, value) => goToParcel(value)}
          renderInput={(params) => {
            const { InputLabelProps, InputProps, ...rest } = params;
            return <InputBase 
                    placeholder="Search…"
                      classes={{
                          root: classes.inputRoot,
                          input: classes.inputInput,
                      }}{...params.InputProps} {...rest}
                      />;
          }}
            />
            </div>) : null }

        {user ? (
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <IconButton
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleMenu}
              color="inherit"
            >
              <Typography style={{ marginRight: '8px', fontFamily: 'inherit' }}>
                {user.first_name}
              </Typography>
              <Avatar
                style={{ backgroundColor: randomColor(user.first_name[0]) }}
              >
                {user.first_name[0]}
              </Avatar>
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={open}
              onClose={handleClose}
              style={{ marginTop: '40px' }}
            >
              <MenuItem onClick={profileViewBtn} href="/user/profile">
                Account
              </MenuItem>
              <MenuItem onClick={parcelsViewBtn}>My Parcels</MenuItem>
              <Divider />
              <MenuItem onClick={logOutBtn}>Log Out</MenuItem>
            </Menu>
          </div>
        ) : (
          <div style={{ display: 'flex' }}>
            <a role="button" href="/user/login" className="startButtons">
              Log In
            </a>
            <a role="button" href="/user/create" className="startButtons">
              Sign up
            </a>
          </div>
        )}
        </div>
      </Toolbar>
    </AppBar>
  );
}
