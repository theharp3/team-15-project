import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { useEffect, useState } from 'react';
import Snackbars from '../Alerts/SnackBars';
import { getAllParcels } from '../../requests/parcels';
import ParcelCard from '../Cards/ParcelCard';
import ParcelsForm from '../Forms/ParcelForm';

const useStyles = makeStyles({
  createBtn: {
    margin: 20,
    backgroundColor: '#e0edd3',
  },
});
export default function Parcels() {
  const classes = useStyles();
  const [parcelsData, setParcelsData] = useState(null);
  const [formVisible, setFormVisible] = useState(false);
  const [parcelId, setParcelId] = useState(null);

  const [successAdd, setSuccessAdd] = useState(null);
  const [successEdit, setSuccessEdit] = useState(null);
  const [failMessage, setFailMessage] = useState(null);

  useEffect(() => {
    getAllParcels().then((res) => setParcelsData(res));
  }, []);

  const editBtn = (id) => {
    setParcelId(id);
    setFormVisible(true);
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  const showForm = () => {
    setFormVisible(true);
  };

  const hideForm = () => {
    if (formVisible) {
      setFormVisible(false);
    }
    setParcelId(null);
  };

  const setSuccessCreate = (data) => {
    setSuccessAdd(data);
    getAllParcels().then((res) => setParcelsData(res));
  };

  const handleSuccessEdit = (data) => {
    setSuccessEdit(data);
    getAllParcels().then((res) => setParcelsData(res));
  };

  const setFail = (message) => {
    setFailMessage(message);
  };

  if (parcelsData === null) {
    return <div>Loading...</div>;
  }

  return (
    <div className={classes.parcelView}>
      {successEdit && (
        <Snackbars
          show={true}
          setToNull={setSuccessEdit}
          severity={'info'}
          text={`Parcel with ID '${successEdit.id} edited!' `}
        />
      )}
      {successAdd && (
        <Snackbars
          show={true}
          setToNull={setSuccessAdd}
          severity={'success'}
          text={`Created parcel with id: ${successAdd.id}!' `}
        />
      )}
      {failMessage && (
        <Snackbars
          show={true}
          setToNull={setFailMessage}
          severity={'error'}
          text={failMessage}
        />
      )}
      <br />
      <Button
        onClick={showForm}
        variant="outlined"
        color="primary"
        className={classes.createBtn}
      >
        Create Parcel +{' '}
      </Button>
      <br />
      {formVisible && (
        <ParcelsForm
          hideForm={hideForm}
          parcelId={parcelId}
          setSuccessCreate={setSuccessCreate}
          onFail={setFail}
          handleSuccessEdit={handleSuccessEdit}
        />
      )}
      <span>
        {parcelsData.map((el) => (
          <ParcelCard
            parcelData={el}
            key={el.id}
            editBtn={editBtn}
            handleSuccessEdit={handleSuccessEdit}
          />
        ))}
      </span>
    </div>
  );
}
