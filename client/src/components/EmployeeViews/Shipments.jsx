import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { useEffect, useState } from 'react';
import ShipmentForm from '../Forms/ShipmentForm';
import Snackbars from '../Alerts/SnackBars';
import { getShipments } from '../../requests/shipments';
import ShipmentCard from '../Cards/ShipmentCard';

const useStyles = makeStyles({
  createBtn: {
    margin: 20,
    backgroundColor: '#e0edd3',
  },
});
export default function Shipments() {
  const classes = useStyles();
  const [shipmentsData, setShipmentsData] = useState(null);
  const [formVisible, setFormVisible] = useState(false);
  const [shipmentId, setShipmentId] = useState(null);

  const [successAdd, setSuccessAdd] = useState(null);
  const [successEdit, setSuccessEdit] = useState(null);
  const [failMessage, setFailMessage] = useState(null);

  useEffect(() => {
    getShipments().then((res) => setShipmentsData(res));
  }, []);

  const editBtn = (id) => {
    setShipmentId(id);
    setFormVisible(true);
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  const showForm = () => {
    setFormVisible(true);
  };

  const hideForm = () => {
    if (formVisible) {
      setFormVisible(false);
    }
    setShipmentId(null);
  };

  const setSuccessCreate = (data) => {
    setSuccessAdd(data);
    getShipments().then((res) => setShipmentsData(res));
  };

  const handleSuccessEdit = (data) => {
    setSuccessEdit(data);
    getShipments().then((res) => setShipmentsData(res));
  };

  const setFail = (message) => {
    setFailMessage(message);
  };

  if (shipmentsData === null || shipmentsData === undefined) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      {successEdit && (
        <Snackbars
          show={true}
          setToNull={setSuccessEdit}
          severity={'info'}
          text={`Shipment with ID '${successEdit.id} edited!' `}
        />
      )}
      {successAdd && (
        <Snackbars
          show={true}
          setToNull={setSuccessAdd}
          severity={'success'}
          text={`Created shipment with id: ${successAdd.id}!' `}
        />
      )}
      {failMessage && (
        <Snackbars
          show={true}
          setToNull={setFailMessage}
          severity={'error'}
          text={failMessage}
        />
      )}
      <br />
      <Button
        onClick={showForm}
        variant="outlined"
        color="primary"
        className={classes.createBtn}
      >
        Create Shipment +{' '}
      </Button>
      <br />
      {formVisible && (
        <ShipmentForm
          hideForm={hideForm}
          shipmentId={shipmentId}
          setSuccessCreate={setSuccessCreate}
          onFail={setFail}
          handleSuccessEdit={handleSuccessEdit}
        />
      )}
      <span>
        {shipmentsData.map((el) => (
          <ShipmentCard
            shipmentsData={el}
            key={el.id}
            editBtn={editBtn}
            handleSuccessEdit={handleSuccessEdit}
          />
        ))}
      </span>
    </div>
  );
}
