import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { useEffect, useState } from 'react';
import { getWarehouses } from '../../requests/warehouses';
import WarehouseCard from '../Cards/WarehouseCard';
import WarehouseForm from '../Forms/WarehouseForm';
import Snackbars from '../Alerts/SnackBars';

const useStyles = makeStyles({
  createBtn: {
    margin: 20,
    backgroundColor: '#e0edd3',
  },
});

export default function Warehouses() {
  const classes = useStyles();
  const [whData, setWhData] = useState(null);
  const [formVisible, setFormVisible] = useState(false);
  const [successAdd, setSuccessAdd] = useState(null);
  const [failMessage, setFailMessage] = useState(null);
  const [successEdit, setSuccessEdit] = useState(null);
  const [warehouseId, setWarehouseId] = useState(null);

  useEffect(() => {
    getWarehouses().then((res) => setWhData(res));
  }, []);

  const editBtn = (id) => {
    setWarehouseId(id);
    setFormVisible(true);
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  const showForm = () => {
    setFormVisible(true);
  };

  const hideForm = () => {
    if (formVisible) {
      setFormVisible(false);
    }
    setWarehouseId(null);
  };

  const setSuccessCreate = (data) => {
    setSuccessAdd(data);
    getWarehouses().then((res) => setWhData(res));
  };

  const handleSuccessEdit = (data) => {
    setSuccessEdit(data);
    getWarehouses().then((res) => setWhData(res));
  };

  const setFail = (message) => {
    setFailMessage(message);
  };

  if (whData === null) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      {successEdit && (
        <Snackbars
          show={true}
          setToNull={setSuccessEdit}
          severity={'info'}
          text={`Warehouse with street name '${successEdit.street_name} edited!' `}
        />
      )}
      {successAdd && (
        <Snackbars
          show={true}
          setToNull={setSuccessAdd}
          severity={'success'}
          text={`Warehouse with street name '${successAdd.street_name} created!' `}
        />
      )}
      {failMessage && (
        <Snackbars
          show={true}
          setToNull={setFailMessage}
          severity={'error'}
          text={failMessage}
        />
      )}
      <br />
      <Button
        onClick={showForm}
        variant="outlined"
        color="primary"
        className={classes.createBtn}
      >
        Create Warehouse +{' '}
      </Button>
      <br />
      {formVisible && (
        <WarehouseForm
          hideForm={hideForm}
          warehouseId={warehouseId}
          setSuccessCreate={setSuccessCreate}
          onFail={setFail}
          handleSuccessEdit={handleSuccessEdit}
        />
      )}
      <br />
      <span>
        {whData.map((el) => (
          <WarehouseCard whData={el} key={el.id} editBtn={editBtn} />
        ))}
      </span>
    </div>
  );
}
