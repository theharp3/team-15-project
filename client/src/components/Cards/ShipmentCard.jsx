import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import { useState } from 'react';
import { SHIPMENTS_CARGO, SHIPMENTS_STATUS } from '../../common/constants';
import { useEffect } from 'react';
import { editShipment } from '../../requests/shipments';
import ParcelsInShipmentList from '../UtilComponents/ParcelsInShipmentList';
import Snackbars from '../Alerts/SnackBars';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    margin: 20,
    backgroundColor: '#bee6e6',
  },
  locationText: {
    marginTop: 5,
    fontSize: 14,
  },
  idText: {
    marginBottom: 5,
    fontSize: 14,
  },

  info: {
    marginRight: 15,
  },
});

export default function ShipmentCard(props) {
  const classes = useStyles();
  const [dispatchBtnDisabled, setDispatchBtnDisabled] = useState(true);
  const [editBtnDisabled, setEditBtnDisabled] = useState(false);

  const [parcelsInShipmentListVisible, setParcelsInShipmentListVisible] =
    useState(false);
  const [listErrorMsg, setListErrorMsg] = useState(null);
  const [cardColor, setCardColor] = useState({ backgroundColor: '#d3eded' });

  useEffect(() => {
    if (
      props.shipmentsData.total_cargo_weight >= SHIPMENTS_CARGO.minWeight &&
      props.shipmentsData.total_cargo_weight <= SHIPMENTS_CARGO.maxWeight &&
      props.shipmentsData.status === 'Preparing'
    ) {
      setDispatchBtnDisabled(false);
    }

    if (
      props.shipmentsData.status === 'En route' ||
      props.shipmentsData.status === 'Delivered'
    ) {
      setEditBtnDisabled(true);
    }

    if (new Date(props.shipmentsData.arrival_date) < new Date()) {
      editShipment(
        { shipment_status_id: SHIPMENTS_STATUS.delivered },
        props.shipmentsData.id,
      );
    }

    if (props.shipmentsData.status === 'En route') {
      setCardColor({ backgroundColor: '#6a9191' });
    }

    if (props.shipmentsData.status === 'Delivered') {
      setCardColor({ backgroundColor: '#8ca3a3' });
    }
  }, [
    props.shipmentsData.total_cargo_weight,
    props.shipmentsData.status,
    props.shipmentsData.arrival_date,
    props.shipmentsData.id,
  ]);

  const dispatchBtnHandler = () => {
    editShipment(
      { shipment_status_id: SHIPMENTS_STATUS.enRoute },
      props.shipmentsData.id,
    ).then((res) => {
      props.handleSuccessEdit(res);
      setDispatchBtnDisabled(true);
    });
  };

  const handleSeeParcelsBtn = (parcelsInShipmentArr) => {
    if (parcelsInShipmentArr.length !== 0) {
      if (parcelsInShipmentListVisible) {
        setParcelsInShipmentListVisible(false);
      } else {
        setParcelsInShipmentListVisible(true);
      }
    }
  };

  return (
    <div>
      {listErrorMsg && (
        <Snackbars
          show={true}
          setToNull={setListErrorMsg}
          severity={'error'}
          text={listErrorMsg}
        />
      )}
      <Card className={classes.root} variant="outlined" style={cardColor}>
        <CardContent>
          <Typography variant="h6" component="h6">
            Shipment id: {props.shipmentsData.id}
          </Typography>
          <div>
            <Typography
              variant="subtitle1"
              color="textSecondary"
              display="inline"
              className={classes.info}
            >
              Origin:{' '}
              <Typography variant="inherit" color="textPrimary">
                {props.shipmentsData.origin_warehouse}
              </Typography>
            </Typography>
            <Typography
              variant="subtitle1"
              color="textSecondary"
              display="inline"
              className={classes.info}
            >
              Departure date:{' '}
              <Typography variant="inherit" color="textPrimary">
                {moment(props.shipmentsData.departure_date).format(
                  'MM/DD/YYYY',
                )}
              </Typography>
            </Typography>
          </div>
          <div>
            <Typography
              className={classes.info}
              variant="subtitle1"
              color="textSecondary"
              display="inline"
            >
              Destination:{' '}
              <Typography variant="inherit" color="textPrimary">
                {props.shipmentsData.destination_warehouse}
              </Typography>
            </Typography>
            <Typography
              className={classes.info}
              variant="subtitle1"
              color="textSecondary"
              display="inline"
            >
              Arrival date:{' '}
              <Typography variant="inherit" color="textPrimary">
                {moment(props.shipmentsData.arrival_date).format('MM/DD/YYYY')}
              </Typography>
            </Typography>
          </div>
          <div>
            <Typography
              className={classes.info}
              variant="subtitle1"
              color="textSecondary"
              display="inline"
            >
              Status:{' '}
              <Typography variant="inherit" color="textPrimary">
                {props.shipmentsData.status}
              </Typography>
            </Typography>
            <Typography
              className={classes.info}
              variant="subtitle1"
              color="textSecondary"
              display="inline"
            >
              Total cargo (kg):{' '}
              <Typography variant="inherit" color="textPrimary">
                {props.shipmentsData.total_cargo_weight}
              </Typography>
            </Typography>
          </div>
        </CardContent>
        <CardActions>
          <Button
            variant="outlined"
            color="primary"
            size="small"
            onClick={() => props.editBtn(props.shipmentsData.id)}
            disabled={editBtnDisabled}
          >
            Edit
          </Button>
          <Button
            variant="outlined"
            color="primary"
            size="small"
            onClick={handleSeeParcelsBtn}
          >
            Cargo Toggle
          </Button>
          <Button
            variant="outlined"
            color="secondary"
            size="small"
            disabled={dispatchBtnDisabled}
            onClick={dispatchBtnHandler}
          >
            {props.shipmentsData.status === 'En route'
              ? 'Dispatched'
              : 'Dispatch'}
          </Button>
        </CardActions>
        {parcelsInShipmentListVisible && (
          <ParcelsInShipmentList
            setParcelsInShipmentListVisible={setParcelsInShipmentListVisible}
            setListErrorMsg={setListErrorMsg}
            shipmentsData={props.shipmentsData}
            handleSuccessEdit={props.handleSuccessEdit}
          />
        )}
      </Card>
    </div>
  );
}
