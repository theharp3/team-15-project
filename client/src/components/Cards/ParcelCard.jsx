import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useState } from 'react';
import AvailableShipmentsList from '../UtilComponents/AvailableShipmentsList';
import {
  getShipmentById,
  getShipmentsWithStatus,
} from '../../requests/shipments';
import { SHIPMENTS_STATUS } from '../../common/constants';
import Snackbars from '../Alerts/SnackBars';
import { useEffect } from 'react';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    margin: 20,
    backgroundColor: '#d3d5ed',
  },
  locationText: {
    marginTop: 5,
    fontSize: 14,
  },
  idText: {
    marginBottom: 5,
    fontSize: 14,
  },

  info: {
    marginRight: 15,
  },
});

export default function ParcelCard(props) {
  const classes = useStyles();
  const [availableShipmentsList, setAvailableShipmentsList] = useState([]);
  const [availableShipmentsListVisible, setAvailableShipmentsListVisible] =
    useState(false);

  const [listErrorMsg, setListErrorMsg] = useState(null);
  const [addToShipmentBtnDisabled, setAddToShipmentBtnDisabled] =
    useState(false);
  const [editBtnDisabled, setEditBtnDisabled] = useState(false);
  const [cardColor, setCardColor] = useState({ backgroundColor: '#d3d5ed' });

  useEffect(() => {
    if (props.parcelData.shipments_id) {
      setCardColor({ backgroundColor: '#8789a1' });
      getShipmentById(props.parcelData.shipments_id).then((res) => {
        if (res.status === 'En route') {
          setAddToShipmentBtnDisabled(true);
          setEditBtnDisabled(true);
          setCardColor({ backgroundColor: '#a9abc4' });
        }
      });
    }
  }, [props.parcelData.shipments_id]);

  const handleAvailableShipmentsBtn = () => {
    getShipmentsWithStatus(SHIPMENTS_STATUS.preparing)
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setAvailableShipmentsList(res);
      })
      .catch((error) => setListErrorMsg(error.message));

    if (availableShipmentsList.length !== 0) {
      if (availableShipmentsListVisible) {
        setAvailableShipmentsListVisible(false);
      } else {
        setAvailableShipmentsListVisible(true);
      }
    }
  };

  return (
    <div>
      {listErrorMsg && (
        <Snackbars
          show={true}
          setToNull={setListErrorMsg}
          severity={'error'}
          text={listErrorMsg}
        />
      )}
      <Card className={classes.root} variant="outlined" style={cardColor}>
        <CardContent>
          <Typography variant="h6" component="h6">
            Parcel id: {props.parcelData.id}
          </Typography>
          <div>
            <Typography
              variant="subtitle1"
              color="textSecondary"
              display="inline"
              className={classes.info}
            >
              Customer:{' '}
              <Typography variant="inherit" color="textPrimary">
                {`${props.parcelData.first_name} ${props.parcelData.last_name}`}
              </Typography>
            </Typography>
            <Typography
              variant="subtitle1"
              color="textSecondary"
              display="inline"
              className={classes.info}
            >
              E-mail:{' '}
              <Typography variant="inherit" color="textPrimary">
                {`${props.parcelData.email}`}
              </Typography>
            </Typography>
          </div>
          <div>
            <Typography
              className={classes.info}
              variant="subtitle1"
              color="textSecondary"
              display="inline"
            >
              City:{' '}
              <Typography variant="inherit" color="textPrimary">
                {props.parcelData.user_city
                  ? props.parcelData.user_city
                  : `N/A`}
              </Typography>
            </Typography>
            <Typography
              className={classes.info}
              variant="subtitle1"
              color="textSecondary"
              display="inline"
            >
              Country:{' '}
              <Typography variant="inherit" color="textPrimary">
                {props.parcelData.user_country
                  ? props.parcelData.user_country
                  : `N/A`}
              </Typography>
            </Typography>
            <Typography
              className={classes.info}
              variant="subtitle1"
              color="textSecondary"
              display="inline"
            >
              Street:{' '}
              <Typography variant="inherit" color="textPrimary">
                {props.parcelData.address ? props.parcelData.address : `N/A`}
              </Typography>
            </Typography>
          </div>
          <div>
            <Typography
              className={classes.info}
              variant="subtitle1"
              color="textSecondary"
              display="inline"
            >
              Category:{' '}
              <Typography variant="inherit" color="textPrimary">
                {props.parcelData.category}
              </Typography>
            </Typography>
            <Typography
              className={classes.info}
              variant="subtitle1"
              color="textSecondary"
              display="inline"
            >
              Parcel wight (kg):{' '}
              <Typography variant="inherit" color="textPrimary">
                {props.parcelData.weight}
              </Typography>
            </Typography>
            <Typography
              className={classes.info}
              variant="subtitle1"
              color="textSecondary"
              display="inline"
            >
              Shipment Id:{' '}
              <Typography variant="inherit" color="textPrimary">
                {props.parcelData.shipments_id}
              </Typography>
            </Typography>
          </div>
        </CardContent>
        <CardActions>
          <Button
            variant="outlined"
            color="primary"
            size="small"
            onClick={() => props.editBtn(props.parcelData.id)}
            disabled={editBtnDisabled}
          >
            Edit
          </Button>
          <Button
            variant="outlined"
            color="primary"
            size="small"
            onClick={() => handleAvailableShipmentsBtn()}
            disabled={addToShipmentBtnDisabled}
          >
            {props.parcelData.shipments_id
              ? 'Change shipment'
              : 'Add to shipment'}
          </Button>
        </CardActions>
        {availableShipmentsListVisible &&
        availableShipmentsList !== null &&
        !availableShipmentsList.message ? (
          <AvailableShipmentsList
            availableShipmentsList={availableShipmentsList}
            setAvailableShipmentsListVisible={setAvailableShipmentsListVisible}
            setListErrorMsg={setListErrorMsg}
            handleSuccessEdit={props.handleSuccessEdit}
            parcelData={props.parcelData}
          />
        ) : null}
      </Card>
    </div>
  );
}
