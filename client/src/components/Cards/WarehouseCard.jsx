import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { useState } from 'react';
import { inboundShipments } from '../../requests/shipments';
import Snackbars from '../Alerts/SnackBars';
import InboundShipmentsList from '../UtilComponents/InboundShipmentsList';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    margin: 20,
    backgroundColor: '#b9d8ed',
  },
  locationText: {
    marginTop: 5,
    fontSize: 14,
  },
  idText: {
    marginBottom: 5,
    fontSize: 14,
  },
});

export default function WarehouseCard(props) {
  const classes = useStyles();

  const [inboundShipmentsList, setInboundShipmentsList] = useState(null);
  const [inboundShipmentsListVisible, setInboundShipmentsListVisible] =
    useState(false);
  const [listErrorMsg, setListErrorMsg] = useState(null);

  const handleInboundShipmentsBtn = (warehouseId) => {
    if (inboundShipmentsListVisible) {
      setInboundShipmentsListVisible(false);
    } else {
      setInboundShipmentsListVisible(true);
    }
    inboundShipments(warehouseId)
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setInboundShipmentsList(res);
      })
      .catch((error) => setListErrorMsg(error.message));
  };

  return (
    <div>
      {listErrorMsg && (
        <Snackbars
          show={true}
          setToNull={setListErrorMsg}
          severity={'error'}
          text={listErrorMsg}
        />
      )}
      <Card className={classes.root} variant="outlined">
        <CardContent>
          <Typography variant="h5" component="h2">
            {props.whData.street_name}
          </Typography>
          <Typography
            className={classes.locationText}
            color="textSecondary"
            gutterBottom
          >
            {props.whData.cities_name} / {props.whData.countries_name}
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            variant="outlined"
            color="primary"
            size="small"
            onClick={() => handleInboundShipmentsBtn(props.whData.id)}
          >
            Inbound Shipments Toggle
          </Button>
          <Button
            variant="outlined"
            color="primary"
            size="small"
            onClick={() => props.editBtn(props.whData.id)}
          >
            Edit
          </Button>
        </CardActions>
        {inboundShipmentsListVisible &&
        inboundShipmentsList !== null &&
        !inboundShipmentsList.message ? (
          <InboundShipmentsList inboundShipmentsList={inboundShipmentsList} />
        ) : null}
      </Card>
    </div>
  );
}
