import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import { BASE_URL } from '../../common/constants';
import { useState, useEffect } from 'react';
import { getAllCustomers } from '../../requests/user';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  footer :{
    display:'flex',
    justifyContent:'space-evenly',
    padding: '5px',
    background:'linear-gradient(rgb(42, 112, 160), rgb(214, 118, 28)) center center / cover',
    bottom: '0',
    left: '0',
    height:'40px'
  },
  text: {
    fontFamily: 'Montserrat, sans-serif!important',
    fontWeight:'bold',
    color:'whitesmoke',
    display:'flex',
    alignItems: 'flex-end',
  }
})
export default function Footer() {
  const classes = useStyles();
  const [customers, setCustomers] = useState(0);
  useEffect(() =>{
    getAllCustomers().then((res)=>{
      setCustomers(res.length);
    })
  },[])

    return (
        <div className={classes.footer}>
        <Typography className={classes.text}>
            Customers IRL : {customers}
        </Typography>
            <Typography variant="body2" align="center" className={classes.text}>
      {'Documentation for the '}
      <Link style={{color:'red', textDecoration: 'none',marginLeft:'5px',marginRight:'5px'}} href={`https://app.swaggerhub.com/apis-docs/azsamdenis/Users-DeliverIT/0.1`}>
        API
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
        </div>
    )
}
