import 'date-fns';
import React from 'react';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { createTheme, makeStyles, ThemeProvider } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
  iconSelect: {
      fill: 'white',
  },
  rootSelect: {
      color: 'white',
  },
  inputLabel: {
    color:'white',
  },
  select: {
    '&:before': {
        borderColor: 'white',
    },
    '&:after': {
        borderColor: 'white',
    },
    '&:not(.Mui-disabled):hover::before': {
        borderColor: 'white',
    },
  },
  cssLabel: {
    color : 'white !important'
  }, 
  notchedOutline: {
    borderWidth: '1px',
    borderColor: 'white !important',
    color: 'white!important',
  },
  line:{
    "&&&:before": {
      borderBottom: "1px solid white"
  },
  "&&:after": {
      borderBottom: "1px solid white"
  }
  }
}));

const theme = createTheme({
  overrides: {
    MuiIconButton: {
      root: {
        color: 'white',
      },
    },
  },
});
export default function DatePicker(props) {
  const classes = useStyles();
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container justifyContent="space-around" className={classes.cssLabel}>
      <ThemeProvider theme={theme}>
        <KeyboardDatePicker
          disableToolbar
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          label={props.label}
          value={props.value}
          onChange={props.dateInputChange}
          className={classes.select}
          InputLabelProps={{
             classes: {
                root: classes.cssLabel,
                focused: classes.cssLabel,
             },
         }}
          InputProps={{
              classes: {
                  root: classes.notchedOutline,
                  focused: classes.notchedOutline,
                  underline: classes.line
                                            
              },
          }}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        </ThemeProvider>
      </Grid>
    </MuiPickersUtilsProvider>
  );
}