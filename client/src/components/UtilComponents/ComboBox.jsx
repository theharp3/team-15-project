import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

export default function ComboBox({
  options,
  optionTitle,
  secondOptionTitle,
  selectedValue,
  label,
  handleComboBox,
  initialValue,
}) {
  return (
    <Autocomplete
      id="combo-box"
      options={options}
      disableClearable={true}
      getOptionLabel={(option) =>
        option ? `${option[optionTitle]} ${option[secondOptionTitle]}` : ``
      }
      value={initialValue}
      style={{ width: 300 }}
      onChange={(event, value) => handleComboBox(value[selectedValue])}
      renderInput={(params) => (
        <TextField {...params} label={label} variant="outlined" />
      )}
    />
  );
}
