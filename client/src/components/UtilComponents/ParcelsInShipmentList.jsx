import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import { getParcelByShipmentId, updateParcel } from '../../requests/parcels';
import { editShipment } from '../../requests/shipments';
import { useState } from 'react';
import { RemoveCircleOutline } from '@material-ui/icons';
import { useEffect } from 'react';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 20,
    width: '100%',
    maxWidth: '70%',
    backgroundColor: '#d3eded',
  },
}));

export default function ParcelsInShipmentList(props) {
  const { shipmentsData, setListErrorMsg } = props;
  const classes = useStyles();
  const [listItemBtnDisabled, setListItemBtnDisabled] = useState(false);
  const [parcelsByShipmentList, setParcelsByShipmentList] = useState([]);
  const [listColor, setListColor] = useState({ backgroundColor: '#d3eded' });

  useEffect(() => {
    if (shipmentsData.status === 'En route') {
      setListItemBtnDisabled(true);
      setListColor({ backgroundColor: '#6a9191' });
    }

    if (shipmentsData.status === 'Delivered') {
      setListColor({ backgroundColor: '#8ca3a3' });
    }
  }, [shipmentsData.status]);

  useEffect(() => {
    getParcelByShipmentId(shipmentsData.id)
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setParcelsByShipmentList(res);
      })
      .catch((error) => setListErrorMsg(error.message));
  }, [setListErrorMsg, shipmentsData.id]);

  const handleListItemBtn = (parcelId, parcelWeight) => {
    const subtractedWeight = shipmentsData.total_cargo_weight - parcelWeight;
    editShipment({ total_cargo_weight: subtractedWeight }, shipmentsData.id)
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }

        props.handleSuccessEdit(res);
      })
      .catch((error) => setListErrorMsg(error.message));

    updateParcel({ shipments_id: null }, parcelId)
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
      })
      .catch((error) => setListErrorMsg(error.message));

    props.setParcelsInShipmentListVisible(false);
  };

  console.log(shipmentsData.status, 'shipmentsDatastatus');

  if (!shipmentsData) {
    return <div>Loading...</div>;
  }

  return (
    <div className={classes.root} style={listColor}>
      <List component="nav" aria-label="main mailbox folders">
        {parcelsByShipmentList.map((e) => (
          <div key={e.id}>
            <Divider />
            <ListItem
              button
              disabled={listItemBtnDisabled}
              onClick={() => handleListItemBtn(e.id, e.weight)}
            >
              <ListItemIcon>
                <RemoveCircleOutline />
              </ListItemIcon>
              <ListItemText primary={`Parcel Id: ${e.id}`} />
              <ListItemText primary={`Total weight: ${e.weight}`} />
              <ListItemText primary={`Category: ${e.category}`} />
            </ListItem>
            <Divider />
          </div>
        ))}
      </List>
    </div>
  );
}
