import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import FlightLandIcon from '@material-ui/icons/FlightLand';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 20,
    width: '100%',
    maxWidth: '70%',
    backgroundColor: '#b9d8ed',
  },
}));

export default function InboundShipmentsList(props) {
  const classes = useStyles();

  const handleListItemBtn = () => {};

  return (
    <div className={classes.root}>
      <List component="nav" aria-label="main mailbox folders">
        {props.inboundShipmentsList.map((e) => (
          <div key={e.id}>
            <Divider />
            <ListItem button onClick={handleListItemBtn} disabled>
              <ListItemIcon>
                <FlightLandIcon />
              </ListItemIcon>
              <ListItemText primary={`Shipment Id: ${e.id}`} />
              <ListItemText primary={`From: ${e.origin_warehouse}`} />
              <ListItemText
                primary={`Arrival: ${moment(e.arrival_date).format(
                  'MM/DD/YYYY',
                )}`}
              />
            </ListItem>
            <Divider />
          </div>
        ))}
      </List>
    </div>
  );
}
