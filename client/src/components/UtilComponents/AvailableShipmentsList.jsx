import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import moment from 'moment';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { updateParcel } from '../../requests/parcels';
import { editShipment, getShipmentById } from '../../requests/shipments';
import { useState } from 'react';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 20,
    width: '100%',
    maxWidth: '70%',
    backgroundColor: '#d3d5ed',
  },
}));

export default function AvailableShipmentsList(props) {
  const classes = useStyles();
  const [currentParcelShipment, setCurrentParcelShipment] = useState(null);
  const [listColor, setListColor] = useState({ backgroundColor: '#d3d5ed' });

  useEffect(() => {
    getShipmentById(props.parcelData.shipments_id).then((res) => {
      setCurrentParcelShipment(res);
    });

    if (props.parcelData.shipments_id) {
      setListColor({ backgroundColor: '#8789a1' });
    }
  }, [props.parcelData.shipments_id]);

  const handleListItemBtn = (shipmentId, totalCargoWeight) => {
    if (props.parcelData.shipments_id !== shipmentId) {
      if (props.parcelData.shipments_id) {
        const subtractedWeight =
          currentParcelShipment.total_cargo_weight - props.parcelData.weight;
        editShipment(
          { total_cargo_weight: subtractedWeight },
          props.parcelData.shipments_id,
        );
        const addedWeight = totalCargoWeight + props.parcelData.weight;
        editShipment({ total_cargo_weight: addedWeight }, shipmentId);
      } else {
        const addedWeight = totalCargoWeight + props.parcelData.weight;
        editShipment({ total_cargo_weight: addedWeight }, shipmentId);
      }
    }

    updateParcel({ shipments_id: shipmentId }, props.parcelData.id)
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        props.setAvailableShipmentsListVisible(false);
        props.handleSuccessEdit(res);
      })
      .catch((error) => props.setListErrorMsg(error.message));
  };

  return (
    <div className={classes.root} style={listColor}>
      <List component="nav" aria-label="main mailbox folders">
        {props.availableShipmentsList.map((e) => (
          <div key={e.id}>
            <Divider />
            <ListItem
              button
              onClick={() => handleListItemBtn(e.id, e.total_cargo_weight)}
            >
              <ListItemIcon>
                <AddCircleOutlineIcon />
              </ListItemIcon>
              <ListItemText primary={`Shipment Id: ${e.id}`} />
              <ListItemText
                primary={`Total cargo weight: ${e.total_cargo_weight}`}
              />
              <ListItemText
                primary={`Departure: ${moment(e.departure_date).format(
                  'MM/DD/YYYY',
                )}`}
              />
            </ListItem>
            <Divider />
          </div>
        ))}
      </List>
    </div>
  );
}
