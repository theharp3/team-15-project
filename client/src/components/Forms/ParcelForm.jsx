import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Button, TextField, Typography } from '@material-ui/core';
import { useEffect, useState } from 'react';
import { getShipmentsWithStatus } from '../../requests/shipments';
import { getAllCustomers } from '../../requests/user';
import {
  createParcel,
  getAllCategories,
  getParcelById,
  updateParcel,
} from '../../requests/parcels';
import { SHIPMENTS_STATUS } from '../../common/constants';

const useStyles = makeStyles((theme) => ({
  form: {
    margin: 20,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 150,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  iconSelect: {
      fill: 'white',
  },
  rootSelect: {
      color: 'white',
  },
  inputLabel: {
    color:'white',
  },
  select: {
    '&:before': {
        borderColor: 'white',
    },
    '&:after': {
        borderColor: 'white',
    },
    '&:not(.Mui-disabled):hover::before': {
        borderColor: 'white',
    },
  },
  cssLabel: {
    color : 'white !important'
  }, 
  notchedOutline: {
    borderWidth: '1px',
    borderColor: 'white !important',
    color: 'white',
  },
}));

export default function ParcelsForm(props) {
  const classes = useStyles();

  const [weightInputError, setWeightInputError] = useState(false);
  const [categoryInputError, setCategoryInputError] = useState(false);
  const [customerInputError, setCustomerInputError] = useState(false);

  const [customerSelect, setCustomerSelect] = useState(null);
  const [categoriesSelect, setCategoriesSelect] = useState(null);
  const [shipmentsSelect, setShipmentsSelect] = useState(null);
  const [shipmentId, setShipmentId] = useState('');
  const [customerFullName, setCustomerFullName] = useState('');

  const [formEntries, setFormEntries] = useState({
    categories_id: 0,
    weight: 0,
    users_id: 0,
  });

  useEffect(() => {
    if (props.parcelId !== null) {
      getParcelById(props.parcelId).then((res) => {
        setFormEntries(res);
        setShipmentId(res.categories_id);
        setCustomerFullName(`${res.first_name} ${res.last_name}`);
      });
    }

    getAllCustomers().then((res) => setCustomerSelect(res));
    getAllCategories().then((res) => setCategoriesSelect(res));
    getShipmentsWithStatus(SHIPMENTS_STATUS.preparing).then((res) =>
      setShipmentsSelect(res),
    );
  }, [props.parcelId]);

  const addFormEntry = (prop, value) => {
    formEntries[prop] = value;

    setFormEntries({ ...formEntries });
  };

  console.log(formEntries);

  // const handleUserComboBox = (value) => {
  //   formEntries.users_id = value;
  //   setFormEntries({ ...formEntries });
  // };

  const handleFormSubmit = (e) => {
    e.preventDefault();

    setWeightInputError(false);
    setCategoryInputError(false);
    setCustomerInputError(false);

    if (formEntries.weight <= 0) {
      setWeightInputError(true);
    }

    if (formEntries.categories_id <= 0) {
      setCategoryInputError(true);
    }

    if (formEntries.users_id <= 0) {
      setCustomerInputError(true);
    }

    if (props.parcelId !== null) {
      updateParcel(formEntries, props.parcelId)
        .then((res) => {
          if (res.message) {
            throw new Error(res.message);
          }
          props.handleSuccessEdit(res);
          props.hideForm();
        })
        .catch((error) => props.onFail(error.message));
    } else {
      createParcel(formEntries)
        .then((res) => {
          if (res.message) {
            throw new Error(res.message);
          }
          props.setSuccessCreate(res);
          props.hideForm();
        })
        .catch((error) => props.onFail(error.message));
    }
  };

  if (
    customerSelect === null ||
    categoriesSelect === null ||
    shipmentsSelect === null
  ) {
    return <div>Loading ...</div>;
  }

  return (
    <form onSubmit={handleFormSubmit} className={classes.form}>
      {props.parcelId && (
        <div>
          <Typography variant="inherit" color="textPrimary">
            {`Editing parcel No: ${props.parcelId}`}
          </Typography>
          <br />
          <br />
        </div>
      )}

      {/* <FormControl variant="outlined" className={classes.formControl}>
        {customerSelect && (
          <ComboBox
            options={customerSelect}
            optionTitle={customerFullName}
            label={'Customers:'}
            handleComboBox={handleUserComboBox}
            selectedValue={'id'}
            secondOptionTitle={'last_name'}
            initialValue={formEntries.users_id}
          />
        )}
      </FormControl> */}

      {/* <FormControl variant="outlined" className={classes.formControl}>
        <Autocomplete
          id="combo-box"
          options={customerSelect}
          disableClearable={true}
          getOptionLabel={(option) =>
            option ? `${option.first_name} ${option.last_name}` : ''
          }
          value={customerSelect[0]}
          style={{ width: 300 }}
          onChange={(event, value) => handleUserComboBox(value.first_name)}
          renderInput={(params) => (
            <TextField {...params} label={'Customers: '} variant="outlined" />
          )}
        />
      </FormControl> */}

      <FormControl variant="standard" className={classes.formControl}>
        <InputLabel className={classes.inputLabel} id="outlined-label">Customer:</InputLabel>
        <Select
          labelId="-outlined-label"
          id="select-outlined"
          value={formEntries.users_id}
          className={classes.select}
          inputProps={{
                classes: {
                    icon: classes.iconSelect,
                    root: classes.rootSelect,
                },
            }}
          onChange={(e) => addFormEntry('users_id', e.target.value)}
          label="Customer"
          error={customerInputError}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {customerSelect.map((el) => (
            <MenuItem value={el.id} key={el.id}>
              {`${el.first_name} ${el.last_name}`}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      <FormControl variant="standard" className={classes.formControl}>
        <InputLabel className={classes.inputLabel} id="outlined-label">Category:</InputLabel>
        <Select
          labelId="-outlined-label"
          id="select-outlined"
          value={formEntries.categories_id}
          error={categoryInputError}
          className={classes.select}
          inputProps={{
                classes: {
                    icon: classes.iconSelect,
                    root: classes.rootSelect,
                },
            }}
          onChange={(e) => addFormEntry('categories_id', e.target.value)}
          label="Category"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {categoriesSelect.map((el) => (
            <MenuItem value={el.id} key={el.id}>
              {el.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      <FormControl>
        <TextField
          className={classes.formControl}
          label="Weight (kg):"
          variant="outlined"
          value={formEntries.weight}
          error={weightInputError}
          helperText={weightInputError && 'Provide weight.'}
          InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
          InputProps={{
            classes: {
            root: classes.notchedOutline,
            focused: classes.notchedOutline,
            notchedOutline: classes.notchedOutline,
           },
                        
          }}
          onChange={(e) => addFormEntry('weight', +e.target.value)}
        />
      </FormControl>

      {/* <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel id="outlined-label">Shipment Id</InputLabel>
        <Select
          labelId="-outlined-label"
          id="select-outlined"
          value={props.parcelId ? shipmentId : ''}
          onChange={(e) => addFormEntry('shipments_id', e.target.value)}
          label="Shipment (optional)"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {shipmentsSelect.map((el) => (
            <MenuItem value={el.id ? el.id : ''} key={el.id}>
              {el.id}
            </MenuItem>
          ))}
        </Select>
      </FormControl> */}

      <FormControl>
        <Button
          className={classes.formControl}
          variant="contained"
          color="primary"
          size="large"
          type="submit"
        >
          {props.parcelId ? 'Edit' : 'Create'}
        </Button>
      </FormControl>

      <FormControl>
        <Button
          className={classes.formControl}
          variant="contained"
          color="secondary"
          size="large"
          onClick={props.hideForm}
        >
          Cancel
        </Button>
      </FormControl>
    </form>
  );
}
