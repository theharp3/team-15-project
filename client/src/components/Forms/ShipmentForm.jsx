import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Button, Typography } from '@material-ui/core';
import { useEffect, useState } from 'react';
import { getWarehouses } from '../../requests/warehouses';
import DatePicker from '../UtilComponents/MuiDatePicker';
import {
  createShipment,
  editShipment,
  getShipmentById,
} from '../../requests/shipments';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
  form: {
    margin: 20,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  iconSelect: {
      fill: 'white',
  },
  rootSelect: {
      color: 'white',
  },
  inputLabel: {
    color:'white',
  },
  select: {
    '&:before': {
        borderColor: 'white',
    },
    '&:after': {
        borderColor: 'white',
    },
    '&:not(.Mui-disabled):hover::before': {
        borderColor: 'white',
    },
  },
  cssLabel: {
    color : 'white !important'
  }, 
}));

export default function ShipmentForm(props) {
  const classes = useStyles();
  const [originError, setOriginError] = useState(false);
  const [destinationError, setDestinationError] = useState(false);
  const [arrivalDateError, setArrivalDateError] = useState(false);

  const [originSelect, setOriginSelect] = useState(null);
  const [destinationSelect, setDestinationSelect] = useState(null);
  const [selectedDepartureDate, setSelectedDepartureDate] = React.useState(
    new Date(),
  );
  const [selectedArrivalDate, setSelectedArrivalDate] = React.useState(
    new Date(),
  );

  const [formEntries, setFormEntries] = useState({
    departure_date: '',
    arrival_date: '',
    origin_warehouse: '',
    destination_warehouse: '',
  });

  useEffect(() => {
    if (props.shipmentId !== null) {
      getShipmentById(props.shipmentId).then((res) => setFormEntries(res));
    }

    getWarehouses().then((res) => {
      setOriginSelect(res);
      setDestinationSelect(res);
    });
  }, [props.shipmentId]);

  const addFormEntry = (prop, value) => {
    formEntries[prop] = value;

    setFormEntries({ ...formEntries });
  };

  const getDepartureDate = (date) => {
    const dateConverted = moment(date).format('YYYY-MM-DD');
    formEntries.departure_date = dateConverted;
    setSelectedDepartureDate(dateConverted);
    setFormEntries({ ...formEntries });
  };

  const getArrivalDate = (date) => {
    const dateConverted = moment(date).format('YYYY-MM-DD');
    formEntries.arrival_date = dateConverted;
    setSelectedArrivalDate(dateConverted);
    setFormEntries({ ...formEntries });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();

    setOriginError(false);
    setDestinationError(false);
    setArrivalDateError(false);

    if (formEntries.origin_warehouse === '') {
      setOriginError(true);
    }

    if (formEntries.destination_warehouse === '') {
      setDestinationError(true);
    }

    if (formEntries.destination_warehouse === formEntries.origin_warehouse) {
      setDestinationError(true);
    }

    if (
      formEntries.arrival_date < formEntries.departure_date &&
      formEntries.arrival_date !== ''
    ) {
      setArrivalDateError(true);
    }

    if (props.shipmentId !== null) {
      editShipment(formEntries, props.shipmentId)
        .then((res) => {
          if (res.message) {
            throw new Error(res.message);
          }
          props.handleSuccessEdit(res);
          props.hideForm();
        })
        .catch((error) => props.onFail(error.message));
    } else {
      createShipment(formEntries)
        .then((res) => {
          if (res.message) {
            throw new Error(res.message);
          }
          props.setSuccessCreate(res);
          props.hideForm();
        })
        .catch((error) => props.onFail(error.message));
    }
  };

  if (originSelect === null || destinationSelect === null) {
    return <div>Loading ...</div>;
  }

  return (
    <form onSubmit={handleFormSubmit} className={classes.form}>
      {props.shipmentId && (
        <div>
          <Typography variant="inherit" color="textPrimary">
            {`Editing shipment No: ${props.shipmentId}`}
          </Typography>
          <br />
          <br />
        </div>
      )}
      <FormControl variant="standard" className={classes.formControl}>
        <InputLabel className={classes.inputLabel} id="outlined-label">From:</InputLabel>
        <Select
          labelId="-outlined-label"
          id="select-outlined"
          value={formEntries.origin_warehouse}
          className={classes.select}
          inputProps={{
                classes: {
                    icon: classes.iconSelect,
                    root: classes.rootSelect,
                },
            }}
          onChange={(e) => addFormEntry('origin_warehouse', e.target.value)}
          label="From"
          error={originError}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {originSelect.map((el) => (
            <MenuItem value={el.street_name} key={el.id}>
              {el.street_name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <FormControl>
        <DatePicker
          label={'Departure date'}
          value={
            props.shipmentId
              ? formEntries.departure_date
              : selectedDepartureDate
          }
          InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
          dateInputChange={getDepartureDate}
        ></DatePicker>
      </FormControl>

      <FormControl variant="standard" className={classes.formControl}>
        <InputLabel className={classes.inputLabel} id="outlined-label">To:</InputLabel>
        <Select
          labelId="-outlined-label"
          id="select-outlined"
          value={formEntries.destination_warehouse}
          error={destinationError}
          className={classes.select}
          inputProps={{
                classes: {
                    icon: classes.iconSelect,
                    root: classes.rootSelect,
                },
            }}
          onChange={(e) =>
            addFormEntry('destination_warehouse', e.target.value)
          }
          label="To"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {destinationSelect.map((el) => (
            <MenuItem value={el.street_name} key={el.id}>
              {el.street_name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <FormControl>
        <DatePicker
          error={arrivalDateError}
          label={'Arrival date'}
          value={
            props.shipmentId ? formEntries.arrival_date : selectedArrivalDate
          }
          dateInputChange={getArrivalDate}
        ></DatePicker>
      </FormControl>
      <FormControl>
        <Button
          className={classes.formControl}
          variant="contained"
          color="primary"
          size="large"
          type="submit"
        >
          {props.shipmentId ? 'Edit' : 'Create'}
        </Button>
      </FormControl>
      <FormControl>
        <Button
          className={classes.formControl}
          variant="contained"
          color="secondary"
          size="large"
          onClick={props.hideForm}
        >
          Cancel
        </Button>
      </FormControl>
    </form>
  );
}
