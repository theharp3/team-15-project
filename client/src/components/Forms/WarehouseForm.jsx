import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Button, TextField, Typography } from '@material-ui/core';
import { useEffect, useState } from 'react';
import { getCities } from '../../requests/locations';
import {
  createWarehouse,
  editWarehouse,
  getWarehouseById,
} from '../../requests/warehouses';

const useStyles = makeStyles((theme) => ({
  form: {
    margin: 20,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  iconSelect: {
      fill: 'white',
  },
  rootSelect: {
      color: 'white',
  },
  inputLabel: {
    color:'white',
  },
  select: {
    '&:before': {
        borderColor: 'white',
    },
    '&:after': {
        borderColor: 'white',
    },
    '&:not(.Mui-disabled):hover::before': {
        borderColor: 'white',
    },
  },
  cssLabel: {
    color : 'white !important'
  }, 
  notchedOutline: {
    borderWidth: '1px',
    borderColor: 'white !important',
    color: 'white',
  },
}));

export default function WarehouseForm(props) {
  const classes = useStyles();
  const [streetError, setStreetError] = useState(false);
  const [cityError, setCityError] = useState(false);
  const [citiesSelect, setCitiesSelect] = useState(null);

  const [formEntries, setFormEntries] = useState({
    street_name: '',
    cities_name: '',
  });

  useEffect(() => {
    if (props.warehouseId !== null) {
      getWarehouseById(props.warehouseId).then((res) => setFormEntries(res));
    }

    getCities().then((res) => setCitiesSelect(res));
  }, [props.warehouseId]);

  const addFormEntry = (prop, value) => {
    formEntries[prop] = value;

    setFormEntries({ ...formEntries });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();

    setStreetError(false);
    setCityError(false);

    if (formEntries.street_name.length < 3) {
      setStreetError(true);
    }

    if (formEntries.cities_name === '') {
      setCityError(true);
    }

    if (props.warehouseId !== null) {
      editWarehouse(formEntries, props.warehouseId)
        .then((res) => {
          if (res.message) {
            throw new Error(res.message);
          }
          props.handleSuccessEdit(res);
          props.hideForm();
        })
        .catch((error) => props.onFail(error.message));
    } else {
      createWarehouse(formEntries)
        .then((res) => {
          if (res.message) {
            throw new Error(res.message);
          }
          props.setSuccessCreate(res);
          props.hideForm();
        })
        .catch((error) => props.onFail(error.message));
    }
  };

  if (citiesSelect === null) {
    return <div>Loading ...</div>;
  }

  return (
    <form onSubmit={handleFormSubmit} className={classes.form}>
      {props.warehouseId && (
        <div>
          <Typography variant="inherit" color="textPrimary">
            {`Editing warehouse No: ${props.warehouseId}`}
          </Typography>
          <br />
          <br />
        </div>
      )}
      <FormControl>
        <TextField
          className={classes.formControl}
          label="Street Name"
          variant="outlined"
          value={formEntries.street_name}
          error={streetError}
          helperText={
            streetError && 'Street name must be at least 3 characters long.'
          }
          InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
          InputProps={{
            classes: {
            root: classes.notchedOutline,
            focused: classes.notchedOutline,
            notchedOutline: classes.notchedOutline,
           },
                        
          }}
          onChange={(e) => addFormEntry('street_name', e.target.value)}
        />
      </FormControl>
      <FormControl variant="standard" className={classes.formControl}>
        <InputLabel className={classes.inputLabel} id="outlined-label">City</InputLabel>
        <Select
          labelId="-outlined-label"
          id="select-outlined"
          value={formEntries.cities_name}
          className={classes.select}
          inputProps={{
                classes: {
                    icon: classes.iconSelect,
                    root: classes.rootSelect,
                },
            }}
          onChange={(e) => addFormEntry('cities_name', e.target.value)}
          label="City"
          error={cityError}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {citiesSelect.map((el) => (
            <MenuItem value={el.cities_name} key={el.id}>
              {el.cities_name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <FormControl>
        <Button
          className={classes.formControl}
          variant="contained"
          color="primary"
          size="large"
          type="submit"
        >
          {props.warehouseId ? 'Edit' : 'Create'}
        </Button>
      </FormControl>
      <FormControl>
        <Button
          className={classes.formControl}
          variant="contained"
          color="secondary"
          size="large"
          onClick={props.hideForm}
        >
          Cancel
        </Button>
      </FormControl>
    </form>
  );
}
