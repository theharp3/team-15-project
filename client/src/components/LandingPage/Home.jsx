import './Home.css';
import { Paper, Tabs, Tab } from '@material-ui/core';
import { useContext, useState } from 'react';
import AuthContext from '../../providers/AuthContext';
import { makeStyles } from '@material-ui/core';
import AvailableWarehouses from '../CustomerViews/AvailableWarehouses';
import AboutUs from '../CustomerViews/AboutUs';
import MyParcels from '../CustomerViews/CustomerParcels';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    width: '100%',
    color:'white'
  },
});
const Home = () => {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const { user } = useContext(AuthContext);
  return (
    <div className="home" style={{ display: 'flex', flexDirection: 'column' }}>
      <Paper className={classes.root}>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="inherit"
          centered
          style={{ background:'linear-gradient( rgb(87, 48, 12),rgb(231, 130, 35)) center center / cover', }}
        >
          <Tab label="Available Warehouses" />
          <Tab label="About" />
          {user?.role === 'Customer' ? <Tab label="My Parcels" /> : null}
        </Tabs>
      </Paper>
      {value === 0 && <AvailableWarehouses />}
      {value === 1 && <AboutUs />}
      {value === 2 && <MyParcels />}
    </div>
  );
};

export default Home;
