
import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useState } from 'react';
import { userLogin } from '../../requests/user';
import { useContext } from 'react';
import AuthContext from '../../providers/AuthContext';
import { decodeToken } from '../../utils/token';
import { useHistory } from 'react-router-dom';
import backGround from '../../assets/background.jpg'

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="/">
        Pace
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: "calc(100vh - 104px)",
  },
  image: {
    backgroundImage: `url(${backGround})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignInSide() {

  const classes = useStyles();
  const history = useHistory();

  const { setUserContext } = useContext(AuthContext);
  const [passwordError, setPasswordError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [check, setCheck] = useState(false);

  const [user, addEntry] = useState({
    email: {
      value: '',
      type: 'email',
      placeholder: 'email...',
    },
    password: {
      value: '',
      type: 'password',
      placeholder: 'password...',
    },
  });

  const loginHandler = (userState) => {
    userLogin(userState)
      .then((data) => {
        if (data.error){
          setPasswordError(true);
          setEmailError(true);
        }else{
          if(check === true){
            setUserContext(decodeToken(data.token));
          localStorage.setItem('token', data.token);
          }else{
            setUserContext(decodeToken(data.token));
            sessionStorage.setItem('token', data.token);
            sessionStorage.setItem('rememberme',true);}
          history.push('/');
        }
      })
  };
  const checked = () =>{
    if(check === false){
      setCheck(true);
    }
    else{
      setCheck(false);
    }
  };

  const loginUserBtn = (event) => {
    event.preventDefault();
    setPasswordError(false);
    setEmailError(false);

    loginHandler(user);
  };

  const loginUserKeyEnter = (event) => {
    if (event.key === 'enter') {
      event.preventDefault();

      loginHandler(user);
    }
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    const updateInput = user[name];
    updateInput.value = value;

    addEntry({ ...user, [name]: updateInput });
  };

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Log in
          </Typography>
          <form className={classes.form} noValidate onSubmit={loginUserBtn} onKeyDown={loginUserKeyEnter}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              error={emailError}
              helperText={emailError === false ? "" : "Invalid email"}
              onChange={handleInputChange}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              error={passwordError}
              helperText={passwordError === false ? "" : "Invalid password"}
              onChange={handleInputChange}
            />
            <FormControlLabel
              control={<Checkbox value="remember" onClick={checked} color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.submit}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="/user/create" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}