import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useState } from 'react';
import { checkEmail, userSubmission } from '../../requests/user';
import { useHistory } from 'react-router-dom';


function Copyright() {
  return (
    <Typography variant="body2" color="inherit" align="center">
      {'Copyright © '}
      <Link color="inherit" href="/">
        Pace
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  cssLabel: {
    color : 'white !important'
  }, 
  notchedOutline: {
    borderWidth: '1px',
    borderColor: 'white !important',
    color: 'white',
  },
}));

export default function SignUp() {
  const classes = useStyles();
  const [firstNameError, setFirstNameError] = useState(false);
  const [lastNameError, setLastNameError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [passwordErrors, setPasswordErrors] = useState('');
  const [emailErrors, setEmailErrors] = useState('');

  const history = useHistory();

  const [user, setUser] = useState({
    first_name: '',
    last_name: '',
    email: '',
    password: '',
  });
  const createUserEntry = (prop, value) => {
    user[prop] = value;

    setUser({ ...user });
  };

  const submitData = (userData) => {
    userSubmission(userData).then((res) => {
      if (res.message) {
        throw new Error(res.message);
      }
      alert('You have been registered ! Now you can log in ');
      history.push('/user/login');
    });
  };

  const createUserBtn = (e) => {
    e.preventDefault();
    let errors = 0;
    let password = [];
    let email = [];

    setEmailError(false);
    setFirstNameError(false);
    setLastNameError(false);
    setPasswordError(false);
    setPasswordErrors('');
    // First Name Validation
    if (user.first_name === '' || user.first_name.length < 3) {
      setFirstNameError(true);
      errors++;
    }
    // Last Name Validation
    if (user.last_name === '' || user.last_name.length < 3) {
      setLastNameError(true);
      errors++;
    }
    //Password Validation
    if (user.password === '' || user.password.length < 8) {
      password.push('Password must be at least 8 characters.');
      setPasswordError(true);
      errors++;
    }
    if (user.password.search(/[a-z]/i) < 0) {
      password.push('Password must contain at least one letter.');
      setPasswordError(true);
      errors++;
    }
    if (user.password.search(/[0-9]/) < 0) {
      password.push('Password must contain at least one digit.');
      setPasswordError(true);
      errors++;
    }
    setPasswordErrors(password.join('\n'));

    //Email Validation
    const emailRegex = /\S+@\S+\.\S+/;
    if (
      user.email === '' ||
      user.email.length < 8 ||
      emailRegex.test(user.email) === false
    ) {
      email.push('Not a valid email.');
      setEmailError(true);
      errors++;
    } else {
      checkEmail(user.email)
        .then((res) => {
          console.log(res)
          if (res[0]) {
            email.push('Email already used.');
            setEmailError(true);
            errors++;
          }
        })
        .then(() => {
          setEmailErrors(email.join(''));
          if (errors === 0) {
            submitData(user);
            history.push('/user/login');
          }
        });
    }
    setEmailErrors(email.join(''));
  };

  return (
    <Container style={{height:'calc(100vh - 168px)'}}component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate onSubmit={createUserBtn}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="off"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="First Name"
                error={firstNameError}
                InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                InputProps={{
                  classes: {
                  root: classes.notchedOutline,
                  focused: classes.notchedOutline,
                  notchedOutline: classes.notchedOutline,
                },
                              
                }}
                helperText={
                  firstNameError === false
                    ? ''
                    : 'Must be at least 3 characters'
                }
                autoFocus
                onChange={(e) => createUserEntry('first_name', e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastName"
                autoComplete="off"
                InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                InputProps={{
                  classes: {
                  root: classes.notchedOutline,
                  focused: classes.notchedOutline,
                  notchedOutline: classes.notchedOutline,
                },
                              
                }}
                error={lastNameError}
                helperText={
                  lastNameError === false ? '' : 'Must be at least 3 characters'
                }
                onChange={(e) => createUserEntry('last_name', e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="off"
                error={emailError}
                InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                InputProps={{
                  classes: {
                  root: classes.notchedOutline,
                  focused: classes.notchedOutline,
                  notchedOutline: classes.notchedOutline,
                },
                              
                }}
                helperText={emailError === false ? '' : emailErrors}
                onChange={(e) => createUserEntry('email', e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssLabel,
                    },
                    }}
                InputProps={{
                  classes: {
                  root: classes.notchedOutline,
                  focused: classes.notchedOutline,
                  notchedOutline: classes.notchedOutline,
                },
                              
                }}
                error={passwordError}
                helperText={passwordError === false ? '' : passwordErrors}
                onChange={(e) => createUserEntry('password', e.target.value)}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            className={classes.submit}
            onSubmit={createUserBtn}
          >
            Sign Up
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="/user/login" color="inherit">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}
