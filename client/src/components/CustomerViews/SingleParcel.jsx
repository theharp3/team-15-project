import { useContext, useEffect, useState } from 'react';
import { getParcelById, updateParcel } from '../../requests/parcels';
import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import { blue, purple } from '@material-ui/core/colors';
import { Paper } from '@material-ui/core';
import AuthContext from '../../providers/AuthContext';
import { useHistory } from 'react-router';
import backGround from '../../assets/boxes.png';
import StepConnector from '@material-ui/core/StepConnector';
import { Link } from 'react-router-dom';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const ColorConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    },
  },
  completed: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    },
  },
  line: {
    height: 2,
    border: 0,
    backgroundColor: '#eaeaf0',
    borderRadius: 1,
  },
})(StepConnector);
function getSteps() {
  return [
    'Choose where it will be delivered',
    'Packaging',
    'On the way',
    'Delivered',
  ];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return 'Choose where it will be delivered...';
    case 1:
      return 'Your parcel is waiting in the warehouse ... Are you sure? You can still switch...';
    case 2:
      return 'Your parcel is coming to you';
    case 3:
      return 'Delivered';
    default:
      return 'Unknown step';
  }
}
const useStyles = makeStyles((theme) => ({
  singleParcelBody:{
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '100%',
  },
  linkParcel:{
    float: 'left',
    position: 'absolute',
    left: '80px',
    top: '120px',
    color:'white',
    textDecoration: 'none',
    display:'flex',
    alignItems:'center'

  },
  main: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    margin: theme.spacing(5),
    minHeight: 'calc(100vh - 194px)',
    backgroundColor: 'rgba(119, 187, 219, 0.651)',
    fontWeight: 'bolder',
  },
  root: {
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: '100px',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  completed: {
    display: 'inline-block',
  },
  navigation: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: '100px',
  },
  steps: {
    backgroundColor: 'transparent',
  },
  step: {
    fontWeight: 'bolder',
  },
}));

const ColorButton = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText(purple[500]),
    backgroundColor: purple[500],
    '&:hover': {
      backgroundColor: purple[700],
    },
  },
}))(Button);
const ColorButton2 = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText(blue[500]),
    backgroundColor: blue[500],
    '&:hover': {
      backgroundColor: blue[700],
    },
  },
}))(Button);

export default function SingleParcel(props) {
  const classes = useStyles();
  const history = useHistory();
  const [activeStep, setActiveStep] = useState(0);
  const { user } = useContext(AuthContext);
  const [steps, setSteps] = useState([]);
  const [singleParcelData, setSingleParcelData] = useState(null);
  if (steps.length === 0) {
    setSteps(getSteps());
  }
  useEffect(() => {
    getParcelById(props.match.params.id).then((res) => {
      setSingleParcelData(res);
      if (res.state_of_delivery === 'delivered') {
        setActiveStep(3);
      }
      if (res.state_of_delivery === 'To address') {
        setActiveStep(1);
      }
      if (res.state_of_delivery === 'In progress') {
        setActiveStep(2);
      }
      if (res.state_of_delivery === 'In warehouse') {
        steps[2] = 'In Warehouse';
        setSteps(steps);
        setActiveStep(2);
      }
    });
  }, [props.match.params.id, steps]);

  const handleNext = () => {
    if (!user.address) {
      alert('Please provide us with your address');
      history.push('/user');
    } else {
      if (activeStep === 1) {
        setActiveStep(2);
      } else {
        updateParcel(
          { state_of_delivery: 'To address' },
          props.match.params.id,
        );
        setActiveStep(1);
      }
    }
  };
  const handleNext2 = () => {
    updateParcel({ state_of_delivery: 'In warehouse' }, props.match.params.id);
    setActiveStep(2);
  };
  if (singleParcelData === null) {
    return <div>Loading...</div>;
  }
  return (
    <Paper elevation={3} className={classes.main}>
      <h1>PARCEL # {+singleParcelData.public_id + +singleParcelData.id}</h1>
      <div className={classes.singleParcelBody}>
      <Link to="/myparcels" color="inherit" className={classes.linkParcel}><ArrowBackIcon/>Return to my parcels...</Link>
        <div>
          <p>Category : {singleParcelData.name}</p>
          <p>Weight : {singleParcelData.weight} kg</p>
          <p>Warehouse now : {singleParcelData.street_name}</p>
        </div>
        <img src={backGround} alt="" style={{ marginRight: '200px' }} />
        <div></div>
      </div>
      <div className={classes.root}>
        <Stepper
          nonLinear
          activeStep={activeStep}
          className={classes.steps}
          connector={<ColorConnector />}
        >
          {steps.map((label, index) => (
            <Step key={label}>
              <StepButton className={classes.step}>{label}</StepButton>
            </Step>
          ))}
        </Stepper>
        {activeStep === 3 ? (
          <h1 className={classes.navigation}>Your parcel is delivered</h1>
        ) : (
          <div className={classes.navigation}>
            <h1 className={classes.instructions}>
              {singleParcelData.state_of_delivery === 'In warehouse'
                ? 'Your parcel is waiting in the warehouse '
                : getStepContent(activeStep)}
            </h1>
            {activeStep <= 1 ? (
              <div
                style={{
                  display: 'flex',
                  width: '600px',
                  marginBottom: '20px',
                  justifyContent: 'space-around',
                }}
              >
                <ColorButton2
                  variant="contained"
                  color="primary"
                  onClick={handleNext}
                  className={classes.margin}
                >
                  To my address
                </ColorButton2>
                <ColorButton
                  variant="contained"
                  color="primary"
                  onClick={handleNext2}
                  className={classes.margin}
                >
                  Leave in the warehouse
                </ColorButton>
              </div>
            ) : null}
          </div>
        )}
      </div>
    </Paper>
  );
}
