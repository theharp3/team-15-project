import { makeStyles } from '@material-ui/core';
import { useContext, useEffect, useState } from 'react';
import AuthContext from '../../providers/AuthContext';
import { filterParcels, getMyParcels } from '../../requests/parcels';
import SimpleParcel from './Customer Cards/SimpleParcel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { getCategories } from '../../requests/categories';
import React from 'react';
import Slider from '@material-ui/core/Slider';

const useStyles = makeStyles((theme) => ({
  simpleParcelContainer: {
    position: 'relative',
    display:'flex',
    flexWrap: 'wrap'
  },
  clickable:{
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: '0',
    left: '0',
    textDecoration: 'none',
    zIndex: '10', 
  },  form: {
    margin: 20,
  },
  search:{
    display:'flex', 
    width:'100%',
    justifyItems:'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  weightBar:{
    width:'250px',
    marginLeft:'30px',
    marginRight:'30px',
  },
  filterBlock:{
    width:'25%'
  },
  formPaper:{
    height: '250px',
    display:'flex',
    minWidth:'75%',
    alignItems:'center',
    marginTop:'100px'
  },
  select: {
    '&:before': {
        borderColor: 'white',
    },
    '&:after': {
        borderColor: 'white',
    },
    '&:not(.Mui-disabled):hover::before': {
        borderColor: 'white',
    },
  },
  iconSelect: {
      fill: 'white',
  },
  rootSelect: {
      color: 'white',
  },
  inputLabel: {
    color:'white',
  },
}));
const states = [{id:1, name:'waiting'},{id:2, name:'In warehouse'},{id:3, name:'Delivered'},{id:4, name:'To address'}]

export default function MyParcels(){
    const classes = useStyles();
    const [parcelData, setParcelData] = useState(null);
    const [categoriesSelect, setCategoriesSelect] = useState(null);
    const [value, setValue] = useState([0, 15]);
    const [formEntries, setFormEntries] = useState({
      name: '',
      state_of_delivery: '',
      min:'',
      max:'',
    });
    const { user } = useContext(AuthContext)

    const handleChange = (event, newValue) => {
      formEntries['min'] = newValue[0];
      formEntries['max'] = newValue[1];
      setFormEntries({ ...formEntries });
      const filters = Object.entries(formEntries).map(([key,value])=> {return `${key}='${value}'`}).filter((el) =>  el.split('=')[1].length > 2 );
      filterParcels(`${filters.join('&')}&users_id=${user.id}`).then((res)=>{
        if(res.message){
          setParcelData([])
        }else{
          setParcelData(res)
        }})

      setValue(newValue);
    };
    const addForm = (prop, value) => {
      if(value !== '' && value?.length > 0){
        formEntries[prop] = value;
        setFormEntries({ ...formEntries });
        const filters = Object.entries(formEntries).map(([key,value])=> {return `${key}='${value}'`}).filter((el) =>  el.split('=')[1].length > 2 );
        filterParcels(`${filters.join('&')}&users_id=${user.id}`).then((res)=>{
        if(res.message){
          setParcelData([])
        }else{
          setParcelData(res)
        }})
      }else{
        setFormEntries({
          name: '',
          state_of_delivery: '',
          min:'',
          max:'',
        })
        getMyParcels(user.id).then((res) =>setParcelData(res));
      }
    };
  
    useEffect(() => {
      getMyParcels(user.id).then((res) =>setParcelData(res));
      getCategories().then((res) =>setCategoriesSelect(res));
    }, []);


    if (parcelData === null) {
        return <div>Loading...</div>;
    }

    if (categoriesSelect === null) {
      return <div>Loading...</div>;
    }

    if(parcelData.message){
      return <h1>YOU HAVE NO PARCELS ATM</h1>
    }


    return (
    <>
    <div className = {classes.search}>
          <FormControl variant="standard" className={classes.formControl}>
        <InputLabel className={classes.inputLabel} id="outlined-label">Category</InputLabel>
        <Select
          labelId="-outlined-label"
          id="select-outlined"
          value={formEntries.name}
          className={classes.select}
          inputProps={{
                classes: {
                    icon: classes.iconSelect,
                    root: classes.rootSelect,
                },
            }}
          onClick={(e) => addForm('name', e.target.value)}
          label="Category"
          error={false}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {categoriesSelect.map((el) => (
            <MenuItem value={el.name} key={el.id}>
              {el.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <div className={classes.weightBar}>
      Weight
      <Slider
        min={0}
        max={100}
        value={value}
        style={{color: 'rgb(231, 130, 35)',}}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
      />
      </div>
      <FormControl variant="standard" className={classes.formControl}>
        <InputLabel className={classes.inputLabel} id="outlined-label">State</InputLabel>
        <Select
          labelId="-outlined-label"
          id="select-outlined"
          value={formEntries.state_of_delivery}
          className={classes.select}
            inputProps={{
                classes: {
                    icon: classes.iconSelect,
                    root: classes.rootSelect,
                },
            }}
          onClick={(e) => addForm('state_of_delivery', e.target.value)}
          label="State"
          error={false}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {states.map((el) => (
            <MenuItem value={el.name} key={el.id}>
              {el.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
    <div className={classes.simpleParcelContainer} >
        {parcelData.length === 0 ?<h2>NO PARCELS WITH THAT CRITERIA</h2>
        : parcelData.map((el) => (<SimpleParcel parcelData={el} key={el.id} />))}
    </div>
    </>)
}