import { Avatar, Paper } from "@material-ui/core";
import { makeStyles } from '@material-ui/core';
import Denis from '../../assets/denis.jpg';
import Asen from '../../assets/asen.png';

const useStyles = makeStyles((theme) => ({
    main:{
        margin: theme.spacing(5),
        minHeight:'700px',
        minWidth:'700px',
        display: 'flex',
        flexDirection:'column',
        alignItems:'center',
        background: 'linear-gradient( 160deg,rgb(86, 93, 99),rgb(198, 213, 223),rgb(198, 213, 223),rgb(198, 213, 223),rgb(86, 93, 99))',
    },
    header:{
        color:'dark-orange',
    },
    large:{
        width:'100px',
        height:'100px'
    },
    people:{
        display:'flex',
        justifyContent:'space-around',
        width:'80%'
    },
    card:{
        display:'flex',
        flexDirection:'column',
        alignItems:'center'
    },
    text:{
        display:'flex',
        width:'500px',
        justifyContent: 'center'}
})
);

export default function AboutUs(){
    const classes = useStyles();
    return (<Paper elevation={8} className={classes.main}>
        <h1 className={classes.header}>ABOUT US</h1>
        <div className={classes.people}>
        <div className={classes.card}>
        <Avatar alt="Denislav Mihalev" src={Denis} className={classes.large} />
        <h3>Denislav Mihalev</h3>
        </div>
        <div className={classes.card}>
        <Avatar alt="Denislav Mihalev" src={Asen} className={classes.large} />
        <h3 style={{fontWeight:'bold'}}>Asen Zhivkov</h3>
        </div>
        </div>
        <h1 className={classes.text} >PACE</h1>
        <h2 className={classes.text}>
            First major project of two young and upcoming programmers.
        </h2>
        <h2 className={classes.text}>
        A web application with amazing styles and functionalities that serves the needs of a freight forwarding company.
        </h2>
    </Paper>)
}