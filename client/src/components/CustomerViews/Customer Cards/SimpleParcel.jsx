import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core';
import Boxes from '../../../assets/boxes.png'

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    margin: 20,
  },
  simpleParcelContainer: {
    position: 'relative',
    display:'flex',
    flexWrap: 'wrap'
  },
  clickable:{
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: '0',
    left: '0',
    textDecoration: 'none', /* No underlines on the link */
    zIndex: '10', /* Places the link above everything else in the div */
  },
  build_badge:{
    display: 'inline-block',
    padding:'0 6px 1px 6px',
    backgroundColor: '#007ACC',
    color: '#fff',
    float: 'right',
    position: 'absolute',
    right: '30px',
    top: '30px',
    borderRadius:'9999px'
  },
});
  const changeColor = (state) =>{
    switch(state) {
      case 'waiting':
        return '#CD5C5C'
      case 'In warehouse':
        return '#1F618D'
      case 'To address':
        return '#D4E543'
      case 'delivered':
          return '#25ED2E'  

      default:
        console.log(`Wrong state!`);
    }

  };


export default function SimpleParcel(props) {
  const classes = useStyles();
  const id = +props.parcelData.public_id + +props.parcelData.id
  return (
    <div className={classes.simpleParcelContainer}>
    <Card variant="outlined" className={classes.root} >
      <CardContent>
      <div>
      <img src={Boxes} alt=''/>
      <span style={{backgroundColor: changeColor(props.parcelData.state_of_delivery)}} className={classes.build_badge}>{props.parcelData.state_of_delivery}</span>
      </div>
        <h2>
          Parcel #{id}
        </h2>
        <div>
        <h3 style={{color:'skyblue',fontWeight:'bolder'}} >{props.parcelData.name}</h3>
        </div>
      </CardContent>
    </Card>
    <a className={classes.clickable} href={`/parcels/${props.parcelData.id}`}/>
    </div>
  );
}