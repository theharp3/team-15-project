import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core';
import Warehouse from '../../../assets/warehouse.png';
const useStyles = makeStyles({
    root: {
      minWidth: 275,
      margin: 20,
      display: 'flex',
      background: 'linear-gradient( 120deg,rgb(86, 93, 99),rgb(198, 213, 223),rgb(198, 213, 223),rgb(198, 213, 223),rgb(86, 93, 99))',

    },
    locationText: {
      color:'black',
    },
  });


export default function SimpleWarehouse(props) {
    const classes = useStyles();
  return (
    <Card variant="outlined" className={classes.root} >
      <img src={Warehouse} alt=''/>
      <CardContent>
        <h2 className={classes.locationText}>
        Warehouse #{props.whData.id}
        </h2>
        <h3 className={classes.locationText}>
          Address : {props.whData.street_name}
        </h3>
        <h3 className={classes.locationText}>
          City : {props.whData.cities_name}
        </h3>
        <h3 className={classes.locationText}>
          Country : {props.whData.countries_name}
        </h3>
      </CardContent>
    </Card>
  );
}