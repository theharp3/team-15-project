import { useEffect, useState } from 'react';
import { getWarehouses, getWarehousesBySearch } from '../../requests/warehouses';
import SimpleWarehouse from './Customer Cards/SimpleWarehouse';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles, Paper, withStyles } from '@material-ui/core';
import { getCities, getCountries, getCitiesByCountry } from '../../requests/locations';
import AnalogClock, { Themes } from 'react-analog-clock';
import '../../App.css';

const WIDTH = 150;
const GMTOFFSET = '1';
const GMTOFFSET2 = '9';
const useStyles = makeStyles((theme) => ({
  form: {
    margin: 20,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  filterBlock:{
    width:'25%',
  },
  formPaper:{
    display:'flex',
    flexDirection:'column',
    maxWidth:'250px',
    alignItems:'center',
    marginTop:'20px',
    background:'inherit',
    border: '2px solid rgb(231, 130, 35)',
    color:'white',
    marginLeft:'30%'
  },
  clocksBlock:{
    width:'25%',
    marginTop:'20px',
    display:'flex',
    flexDirection:'column',
    alignItems:'center'
  },
  select: {
    '&:before': {
        borderColor: 'white',
    },
    '&:after': {
        borderColor: 'white',
    },
    '&:not(.Mui-disabled):hover::before': {
        borderColor: 'white',
    },
  },
  iconSelect: {
      fill: 'white',
  },
  rootSelect: {
      color: 'white',
  },
  inputLabel: {
    color: 'white',
  },
}));
export default function AvailableWarehouses(){
  const classes = useStyles();
    const [whData, setWhData] = useState(null);
    const [citiesSelect, setCitiesSelect] = useState(null);
    const [countriesSelect, setCountriesSelect] = useState(null);
    const [formEntries, setFormEntries] = useState({
      country: '',
      city: '',
    });


    const addFormEntryCountry = (prop, value) => {
      if(typeof value === 'string' && value.length > 0){
      formEntries[prop] = value;

      getCitiesByCountry(value).then((res)=> {
        if(!res.message){
        setCitiesSelect(res)}
      })
      getWarehousesBySearch(`countries_name=${value}`).then((res)=>{
        if(res.message){
          setWhData([])
        }else{
        setWhData(res)
        setFormEntries({ ...formEntries });}})
      }
      else{
        setFormEntries({
          country: '',
          city: '',
        })
        getWarehouses().then((res) =>setWhData(res));
        getCities().then((res) => setCitiesSelect(res));
      }
    };


    const addFormEntryCity = (prop, value) => {
      if(typeof value === 'string' && value.length > 0){
      formEntries[prop] = value;
      getWarehousesBySearch(`cities_name=${value}`).then((res)=>{
        if(res.message){
          setWhData([])
        }else{
          setWhData(res)
          setFormEntries({ ...formEntries })
        }});
      }else{
        setFormEntries({
          country: '',
          city: '',
        })
        getWarehouses().then((res) =>setWhData(res));
      }
    };
  useEffect(() => {
    getWarehouses().then((res) =>setWhData(res));
    getCities().then((res) => setCitiesSelect(res));
    getCountries().then((res) => setCountriesSelect(res));
  }, []);

  if (whData === null || citiesSelect === null || countriesSelect === null) {
    return <div>Loading...</div>;
  }
    return (
      <div style={{display:'flex',width:'100%',justifyContent: 'center'}}>
      <div className={classes.filterBlock}>
      <Paper className={classes.formPaper}>
      <h2 style={{marginTop:'10px'}}>Search by : </h2>
      <FormControl variant="standard" className={classes.formControl}>
        <InputLabel className={classes.inputLabel} id="label">Country</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={formEntries.country}
          className={classes.select}
            inputProps={{
                classes: {
                    icon: classes.iconSelect,
                    root: classes.rootSelect,
                },
            }}
          onClick={(e) => addFormEntryCountry('country', e.target.value)}
          label="Country"
          error={false}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {countriesSelect.map((el) => (
            <MenuItem value={el.countries_name} key={el.id}>
              {el.countries_name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <FormControl variant="standard" style={{paddingBottom: '50px',color:'white'}} className={classes.formControl}>
        <InputLabel className={classes.inputLabel} id="validation-outlined-input">City</InputLabel>
        <Select
          labelId="-outlined-label"
          id="select-outlined"
          value={formEntries.city}
          className={classes.select}
            inputProps={{
                classes: {
                    icon: classes.iconSelect,
                    root: classes.rootSelect,
                },
            }}
          onClick={(e) => addFormEntryCity('city', e.target.value)}
          label="City"
          error={false}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {citiesSelect.map((el) => (
            <MenuItem value={el.cities_name} key={el.id}>
              {el.cities_name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      </Paper>
      </div>
    <div style ={{flexWrap: 'wrap',flexDirection:'height',justifyContent:' space-around',width:'1000px',marginLeft:'50px',marginRight:'50px'}}>    
    {whData.length === 0 ? <h1 style ={{width:'800px',display:'flex',alignItems:'center',justifyContent:'center',maxHeight:'200px'}}>No available warehouses</h1> : 
    whData?.map((el) => (<div key={el.id}>
    <SimpleWarehouse whData={el} />
    </div>))}
    </div>
    <div className={classes.clocksBlock}>
    <AnalogClock width={WIDTH} theme={Themes.navy} />    
    <h1>Sofia</h1>
    <AnalogClock width={WIDTH} theme={Themes.navy} gmtOffset={GMTOFFSET} />    
    <h1>London</h1>
    <AnalogClock width={WIDTH} theme={Themes.navy} gmtOffset={GMTOFFSET2} />
    <h1>Tokyo</h1>
    </div>
    </div>)

};