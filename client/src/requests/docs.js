import { BASE_URL } from '../common/constants';
export const getDocs = () =>{
  return fetch(`${BASE_URL}docs`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  })
}