import { BASE_URL } from '../common/constants';

export const getWarehouses = () => {
  return fetch(`${BASE_URL}warehouses`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const getWarehousesBySearch = (searchParams) => {
  return fetch(`${BASE_URL}warehouses?${searchParams}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const getWarehouseById = (id) => {
  return fetch(`${BASE_URL}warehouses/?warehouseId=${id}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const createWarehouse = (data) => {
  return fetch(`${BASE_URL}warehouses`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  }).then((res) => res.json());
};

export const editWarehouse = (data, id) => {
  return fetch(`${BASE_URL}warehouses/${id}`, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  }).then((res) => res.json());
};

export const deleteWarehouse = (id) => {
  return fetch(`${BASE_URL}warehouses/${id}`, {
    method: 'DELETE',
  }).then((res) => res.json());
};
