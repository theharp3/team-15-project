import { BASE_URL, USER_PAYLOAD } from '../common/constants';

export const getUserInfo = (id) => {
  return fetch(`${BASE_URL}users/?username=${USER_PAYLOAD.username}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};
export const getAllCustomers = () =>{
  return fetch(`${BASE_URL}users`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
}
export const userSubmission = (submissionData) => {
  return fetch(`${BASE_URL}users/`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(submissionData),
  }).then((res) => res.json());
};

export const userLogin = (loginData) => {
  const bodyData = {
    email: loginData.email.value,
    password: loginData.password.value,
  };

  return fetch(`${BASE_URL}users/login`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(bodyData),
  }).then((res) => res.json());
};

export const findUsers = (searchTerm) => {
  return fetch(`${BASE_URL}users/name/?firstName=${searchTerm}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const getPersonInfo = (id) => {
  return fetch(`${BASE_URL}users/${id}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const checkEmail = (email) => {
  return fetch(`${BASE_URL}users/?email=${email}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const checkPass = (email, old_password) => {
  const bodyData = {
    email: email,
    password: old_password,
  };
  return fetch(`${BASE_URL}users/check`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(bodyData),
  }).then((res) => res.json());
};

export const updateUser = (data,id) =>{
  return fetch(`${BASE_URL}users/${id}`, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  }).then((res) => res.json());
}