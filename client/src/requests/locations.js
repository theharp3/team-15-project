import { BASE_URL } from '../common/constants';

export const getCities = () => {
  return fetch(`${BASE_URL}locations/cities`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};
export const getCountries = () => {
  return fetch(`${BASE_URL}locations/countries`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};
export const getCitiesByCountry = (country) =>{
  return fetch(`${BASE_URL}locations/cities?country=${country}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};
