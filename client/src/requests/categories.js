import { BASE_URL } from '../common/constants';

export const getCategories = () => {
  return fetch(`${BASE_URL}categories`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};