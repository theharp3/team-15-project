import { BASE_URL } from '../common/constants';

export const getShipments = () => {
  return fetch(`${BASE_URL}shipments`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const getShipmentsWithStatus = (status) => {
  return fetch(`${BASE_URL}shipments/?status=${status}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const createShipment = (data) => {
  return fetch(`${BASE_URL}shipments`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  }).then((res) => res.json());
};

export const getShipmentById = (id) => {
  return fetch(`${BASE_URL}shipments/?shipmentId=${id}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const editShipment = (data, id) => {
  return fetch(`${BASE_URL}shipments/${id}`, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  }).then((res) => res.json());
};

export const inboundShipments = (id) => {
  return fetch(`${BASE_URL}shipments/?inboundWarehouseId=${id}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};
