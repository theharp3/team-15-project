import { BASE_URL } from '../common/constants';

export const getMyParcels = (id) => {
  return fetch(`${BASE_URL}parcels?users_id=${id}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const getParcelById = (id) => {
  return fetch(`${BASE_URL}parcels/${id}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const getAllParcels = () => {
  return fetch(`${BASE_URL}parcels/cards`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};
export const updateParcel = (data, id) => {
  return fetch(`${BASE_URL}parcels/${id}`, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  }).then((res) => res.json());
};

export const getAllCategories = () => {
  return fetch(`${BASE_URL}parcels/categories`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const createParcel = (data) => {
  return fetch(`${BASE_URL}parcels`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  }).then((res) => res.json());
};

export const filterParcels = (criteria) => {
  return fetch(`${BASE_URL}parcels?${criteria}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};

export const getParcelByShipmentId = (id) => {
  return fetch(`${BASE_URL}parcels/shipments/${id}`, {
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
  }).then((res) => res.json());
};
