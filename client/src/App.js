import Header from './components/Header/Header';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import AuthContext from './providers/AuthContext.js';
import Home from './components/LandingPage/Home';
import { useState, useEffect } from 'react';
import SignUp from './components/SignPages/SignUp';
import SignInSide from './components/SignPages/SignIn';
import './App.css';
import { decodeToken } from './utils/token';
import Footer from './components/Footer/Footer';
import Warehouses from './components/EmployeeViews/Warehouses';
import Shipments from './components/EmployeeViews/Shipments';
import Parcels from './components/EmployeeViews/Parcels';
import EmployeeRoute from './providers/EmployeeRouteGuard';
import UserProfile from './components/User-Profile/UserProfile';
import UserRoute from './providers/UserRouteGuard';
import CustomerRoute from './providers/CustomerRoute';
import SingleParcel from './components/CustomerViews/SingleParcel';
import MyParcels from './components/CustomerViews/CustomerParcels';
function App() {
  const [user, setUserContext] = useState(
    decodeToken(localStorage.getItem('token') || sessionStorage.getItem('token')),
  );
  const [cookie, setCookie] = useState(localStorage.getItem('cookie'));
  //delete cache!
  useEffect(() => {
    if (
      localStorage.getItem('rememberme') === 'true' &&
      localStorage.getItem('token') !== null
    ) {
      let token = decodeToken(localStorage.getItem('token'));
      if (token.exp < Date.now() / 1000) {
        localStorage.clear();
        setUserContext(null);
      }
    }
  }, []);

  const setToken = () => {
    localStorage.setItem('cookie', 'true');
    setCookie('true');
  };

  return (
    <>
      <AuthContext.Provider value={{ user, setUserContext }}>
        <div
          className="App"
          style={{
            backgroundColor: '#f7f7f7',
            display: 'flex',
            flexDirection: 'column',
            minHeight: '100%',
          }}
        >
          <BrowserRouter>
            <Header className="header" />
            <div className="main">
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/user/login" component={SignInSide} />
                <Route exact path="/user/create" component={SignUp} />
                <UserRoute
                  exact
                  path="/user"
                  auth={user}
                  component={UserProfile}
                />
                <EmployeeRoute
                  exact
                  path="/employee/warehouses"
                  auth={user}
                  component={Warehouses}
                />
                <EmployeeRoute
                  exact
                  path="/employee/Shipments"
                  auth={user}
                  component={Shipments}
                />
                <EmployeeRoute
                  exact
                  path="/employee/parcels"
                  auth={user}
                  component={Parcels}
                />
                <CustomerRoute
                  exact
                  path="/parcels/:id"
                  auth={user}
                  component={SingleParcel}
                />
                <CustomerRoute
                  exact
                  path="/myparcels"
                  auth={user}
                  component={MyParcels}
                />
              </Switch>
            </div>
            <Footer className="footer" />
          </BrowserRouter>
          {cookie !== 'true' ? (
            <div className="cookie-container">
              <p>
                We use tokens in this website to give you the best experience on
                our site and show you relevant information. To find out more,
                read our
                <a href="#"> privacy policy</a> and <a href="#">token policy</a>
                .
              </p>

              <button className="cookie-btn" onClick={setToken}>
                Okay
              </button>
            </div>
          ) : null}
        </div>
      </AuthContext.Provider>
    </>
  );
}

export default App;
