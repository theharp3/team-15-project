import decode from 'jwt-decode';

export const decodeToken = (token) => {
  try {
    const user = decode(token);
    return user;
  } catch {}
  return null;
};
