export default {
  minFieldLength: 0,
  // a patter for a password not still implemented
  // "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
  // At least one number
  // At least one uppercase
  // At least one lowercase
  // Minimum 8 characters
};
