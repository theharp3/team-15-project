import express from 'express';
import errors from '../services/errors.js';
import {
  getAllCities,
  getAllCountries,
  getCitiesFromCountry,
} from '../services/locations-service.js';
import * as locationsData from '../data/locations-data.js';

const locationsRoute = express.Router();
locationsRoute.get('/countries', async (req, res) => {
  const { error, data } = await getAllCountries(locationsData)();

  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: 'No entries in DB table!' });
  }

  return res.status(200).json(data);
});

locationsRoute.get('/cities', async (req, res) => {
  if (req.query.country !== undefined && req.query.country !== '') {
    const { error, data } = await getCitiesFromCountry(locationsData)(req.query.country);
    if (error === errors.NOT_FOUND) {
      return res.status(404).json({ message: 'No entries in DB table!' });
    }

    return res.status(200).json(data);
  }
  const { error, data } = await getAllCities(locationsData)();

  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: 'No entries in DB table!' });
  }

  return res.status(200).json(data);
});

export default locationsRoute;
