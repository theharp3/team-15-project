/* eslint-disable import/extensions */
import express from 'express';
import validateBody from '../middlewares/validate-body.js';
import {
  createUser,
  getAllCustomers,
  getUserByEmail,
  validateUser,
  getUserByID,
  getUserByName,
  getUserBySearchTerm,
  updateUser,
} from '../services/user-services.js';
import createUserValidator from '../validators/create-user-validator.js';
import * as usersData from '../data/user-data.js';
import errors from '../services/errors.js';
import createToken from '../auth/create-token.js';
import { blacklistToken } from '../services/token-service.js';
import tokenData from '../data/token-data.js';
import { authMiddleware } from '../auth/auth.middleware.js';
import loggedUserGuard from '../middlewares/user-role-guards.js';

const usersRoute = express.Router();

usersRoute.get('/', async (req, res) => {
  const resultUsers = [];
  const resultIds = [];
  if (req.query.email) {
    const { error, data } = await getUserByEmail(usersData)(req.query.email);

    if (error === errors.NOT_FOUND) {
      return res
        .status(401)
        .json({ message: 'User with that email not found!' });
    }
    if (data[1]) {
      data.map((el) => {
        if (!resultIds.includes(el.id)) {
          resultUsers.push(el);
          resultIds.push(el.id);
        }
      });
    } else if (!resultIds.includes(data.id)) {
      resultIds.push(data.id);
      resultUsers.push(data);
    }
  }
  if (req.query.firstName || req.query.lastName) {
    const { firstName, lastName } = req.query;
    const { error, data } = await getUserByName(usersData)(firstName, lastName);

    if (error === errors.NOT_FOUND) {
      return res
        .status(401)
        .json({ message: 'Users with that first or last name not found!' });
    }
    if (data[1]) {
      data.map((el) => {
        if (!resultIds.includes(el.id)) {
          resultUsers.push(el);
          resultIds.push(el.id);
        }
      });
    } else if (!resultIds.includes(data.id)) {
      resultIds.push(data.id);
      resultUsers.push(data);
    }
  }
  if (req.query.search) {
    const { search } = req.query;
    const { error, data } = await getUserBySearchTerm(usersData)(search);
    if (error === errors.NOT_FOUND) {
      return res
        .status(401)
        .json({ message: 'Users with that first or last name not found!' });
    }
    if (data[1]) {
      data.map((el) => {
        if (!resultIds.includes(el.id)) {
          resultUsers.push(el);
          resultIds.push(el.id);
        }
      });
    } else if (!resultIds.includes(data.id)) {
      resultIds.push(data.id);
      resultUsers.push(data);
    }
  }

  if (Object.keys(req.query).length === 0) {
    const data = await getAllCustomers(usersData)();
    data.map((el) => {
      resultUsers.push(el);
    });
  }

  return res.status(200).json(resultUsers);
});
usersRoute.post('/', validateBody(createUserValidator), async (req, res) => {
  const { error, data } = await createUser(usersData)(req.body);

  if (error === errors.DUPLICATE_RECORD) {
    return res.status(400).json({
      message: `User with email '${req.body.email}' already exists!`,
    });
  }

  res.status(200).json(data);
});
usersRoute.post('/login', async (req, res) => {
  try {
    const user = await validateUser(usersData)(req.body);

    if (user) {
      const token = createToken({
        id: user.id,
        first_name: user.first_name,
        last_name: user.last_name,
        createdOn: user.createdOn,
        email: user.email,
        role: user.role,
        user_country: user.user_country,
        user_city: user.user_city,
        post_code: user.post_code,
        address: user.address,
      });
      res.status(200).json({ token });
    } else {
      res.status(401).json({ error: 'Invalid credentials!' });
    }
  } catch (error) {
    res.status(401).json({ error: error.message });
  }
});
usersRoute.post('/check', async (req, res) => {
  try {
    const user = await validateUser(usersData)(req.body);
    if (user) {
      res.status(200).json({ message: 'Success' });
    } else {
      res.status(401).json({ error: 'Invalid password!' });
    }
  } catch (error) {
    res.status(401).json({ error: error.message });
  }
});

usersRoute.delete('/logout', async (req, res) => {
  const token = req.headers.authorization.replace('Bearer ', '');
  await blacklistToken(tokenData)(token);

  res.status(200).json({ message: 'You have been logged out!' });
});

usersRoute.put('/:id', async (req, res) => {
  const changes = req.body;
  const { error, data } = await updateUser(usersData)(changes, +req.params.id);
  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: 'User not found!' });
  }
  if (data) {
    const token = createToken({
      id: data.id,
      first_name: data.first_name,
      last_name: data.last_name,
      createdOn: data.createdOn,
      email: data.email,
      role: data.role,
      user_country: data.user_country,
      user_city: data.user_city,
      post_code: data.post_code,
      address: data.address,
    });
    res.status(200).json({ token });
  }
});

usersRoute.get('/:id', authMiddleware, loggedUserGuard, async (req, res) => {
  const id = +req.params.id;
  const { error, data } = await getUserByID(usersData)(id);

  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: 'User not found!' });
  }

  return res.json(data);
});
export default usersRoute;
