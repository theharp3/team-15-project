import express from 'express';
import * as warehouseData from '../data/warehouse-data.js';
import {
  createWarehouses,
  deleteWarehouses,
  getAllWarehouses,
  getWarehouseById,
  updateWarehouses,
  filterWarehouse,
} from '../services/warehouse-services.js';
import errors from '../services/errors.js';
import bodyValidator from '../middlewares/validate-body.js';
import warehouseValidator from '../validators/warehouse-validator.js';

const warehousesRoute = express.Router();
/**
 * @swagger
 * /warehouses:
 *   get:
 *     summary: Retrieve a list of current warehouses.
 *     description: Retrieve a list of warehouses from deliverit_db.
 *     responses:
 *       200:
 *         description: A list of warehouses.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 */

warehousesRoute.get('/', async (req, res) => {
  if (req.query.warehouseId) {
    const { error, data } = await getWarehouseById(warehouseData)(
      req.query.warehouseId,
    );
    if (error === errors.NOT_FOUND) {
      return res.status(404).json({ message: `Not Found!` });
    }

    return res.status(200).json(data);
  }
  if (req.query) {
    const filterCriteria = Object.entries(req.query)
      .map(([key, value]) => `${key.split('_')[0]}.${key} = '${value}'`)
      .filter((el) => el.length > 0);
    const { error, data } = await filterWarehouse(warehouseData)(filterCriteria);
    if (error === errors.NOT_FOUND) {
      return res
        .status(404)
        .json({ message: `Parcel with that criteria Not Found!` });
    }
    return res.status(200).json(data);
  }

  const { error, data } = await getAllWarehouses(warehouseData)();

  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: `Not Found!` });
  }

  return res.status(200).json(data);
});

/**
 * @swagger
 * /warehouses:
 *   post:
 *     summary: Create warehouse.
 *     description: Create warehouse.
 *     responses:
 *       200:
 *         description: Successful creation.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 */
warehousesRoute.post(
  '/',
  bodyValidator(warehouseValidator),
  async (req, res) => {
    const { error, data } = await createWarehouses(warehouseData)(req.body);

    if (error === errors.DUPLICATE_RECORD) {
      return res.status(400).json({ message: 'Street name already exists' });
    }

    return res.status(200).json(data);
  },
);

warehousesRoute.put(
  '/:id',
  bodyValidator(warehouseValidator),
  async (req, res) => {
    const { error, data } = await updateWarehouses(warehouseData)(
      req.body,
      req.params.id,
    );

    if (error === errors.DUPLICATE_RECORD) {
      return res.status(400).json({ message: 'Street name already exists' });
    }

    return res.status(200).json(data);
  },
);

warehousesRoute.delete('/:id', async (req, res) => {
  const { error, data } = await deleteWarehouses(warehouseData)(req.params.id);

  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: `Not Found!` });
  }

  return res.status(200).json(data);
});

export default warehousesRoute;
