import express from 'express';
import errors from '../services/errors.js';
import getAllCategories from '../services/categories-services.js';
import * as categoriesData from '../data/categories-data.js';

const categoriesRoute = express.Router();
categoriesRoute.get('/', async (req, res) => {
  const { error, data } = await getAllCategories(categoriesData)();

  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: 'No entries in DB table!' });
  }

  return res.status(200).json(data);
});
export default categoriesRoute;
