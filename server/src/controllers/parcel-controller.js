import express from 'express';
import errors from '../services/errors.js';
import * as parcelData from '../data/parcel-data.js';
import parcelValidators from '../validators/parcel-validator.js';
import bodyValidator from '../middlewares/validate-body.js';
import {
  createParcel,
  deleteParcel,
  getAllParcels,
  getParcelById,
  updateParcel,
  filterAndSortParcels,
  getAllParcelsCards,
  getAllParcelsCategories,
  getAllParcelsByShipment,
} from '../services/parcel-services.js';

const parcelsRoute = express.Router();

parcelsRoute.get('/', async (req, res) => {
  if (Object.keys(req.query).length === 0) {
    const { data } = await getAllParcels(parcelData)();
    return res.status(200).json(data);
  }
  // ex./parcels?sortWeight=ASC&categories_id=2&warehouses_origin=2&sortDue=ASC&min=3

  const sortingCriteria = [];
  const filterCriteria = Object.entries(req.query)
    .map(([key, value]) => {
      if (key === 'sortWeight' || key === 'sortDue') {
        sortingCriteria.push(
          `${key.substring(4).toLocaleLowerCase()} ${value}`,
        );
        return '';
      }
      if (key === 'min') {
        return `weight > ${value}`;
      }
      if (key === 'name') {
        return `categories.name = ${value}`;
      }
      if (key === 'max') {
        return `weight < ${value}`;
      }
      return `${key} = ${value}`;
    })
    .filter((el) => el.length > 0);

  const { error, data } = await filterAndSortParcels(parcelData)(
    filterCriteria,
    sortingCriteria,
  );
  if (error === errors.NOT_FOUND) {
    return res
      .json({ message: `Parcel with that criteria Not Found!` });
  }
  return res.status(200).json(data);
});

parcelsRoute.get('/cards', async (req, res) => {
  const { error, data } = await getAllParcelsCards(parcelData)();
  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: `Not Found!` });
  }
  return res.status(200).json(data);
});

parcelsRoute.get('/categories', async (req, res) => {
  const { error, data } = await getAllParcelsCategories(parcelData)();
  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: `Not Found!` });
  }
  return res.status(200).json(data);
});

parcelsRoute.get('/shipments/:id', async (req, res) => {
  const { error, data } = await getAllParcelsByShipment(parcelData)(
    req.params.id,
  );
  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: `No parcels in this shipment!` });
  }
  return res.status(200).json(data);
});

parcelsRoute.get('/:id', async (req, res) => {
  const { id } = req.params;
  const { data } = await getParcelById(parcelData)(id);
  return res.status(200).json(data);
});

parcelsRoute.post('/', bodyValidator(parcelValidators), async (req, res) => {
  const { error, data } = await createParcel(parcelData)(req.body);

  if (error === errors.NOT_FOUND) {
    return res.status(400).json({ message: `Failed to create parcel` });
  }
  return res.status(200).json(data);
});

parcelsRoute.put('/:id', async (req, res) => {
  const { id } = req.params;
  const { error, data } = await updateParcel(parcelData)(req.body, id);

  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: `Parcel Not Found!` });
  }

  return res.status(200).json(data);
});

parcelsRoute.delete('/:id', async (req, res) => {
  const { id } = req.params;
  const { error, data } = await deleteParcel(parcelData)(id);

  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: `Parcel Not Found!` });
  }

  return res.status(200).json(data);
});

export default parcelsRoute;
