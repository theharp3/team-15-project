import express from 'express';
import errors from '../services/errors.js';
import {
  createShipments,
  deleteShipments,
  getAllShipments,
  getAllShipmentsByCustomerId,
  getAllShipmentsByWarehouseId,
  getAllShipmentsWithStatus,
  getInboundShipments,
  getShipmentById,
  updateShipments,
} from '../services/shipments-service.js';
import * as shipmentsData from '../data/shipments-data.js';
import bodyValidator from '../middlewares/validate-body.js';
import shipmentsValidator from '../validators/shipments-validator.js';

const shipmentsRoute = express.Router();
shipmentsRoute.get('/', async (req, res) => {
  if (req.query.status) {
    const { error, data } = await getAllShipmentsWithStatus(shipmentsData)(
      req.query.status,
    );

    if (error === errors.NOT_FOUND) {
      return res.status(404).json({ message: `No inbound shipments!` });
    }

    return res.status(200).json(data);
  }

  if (req.query.inboundWarehouseId) {
    const { error, data } = await getInboundShipments(shipmentsData)(
      req.query.inboundWarehouseId,
    );

    if (error === errors.NOT_FOUND) {
      return res.status(404).json({ message: `No inbound shipments!` });
    }

    return res.status(200).json(data);
  }

  if (req.query.warehouseId) {
    const { error, data } = await getAllShipmentsByWarehouseId(shipmentsData)(
      req.query.warehouseId,
    );

    if (error === errors.NOT_FOUND) {
      return res.status(404).json({ message: `Not Found!` });
    }

    return res.status(200).json(data);
  }

  if (req.query.customerId) {
    const { error, data } = await getAllShipmentsByCustomerId(shipmentsData)(
      req.query.customerId,
    );

    if (error === errors.NOT_FOUND) {
      return res.status(404).json({ message: `Not Found!` });
    }

    return res.status(200).json(data);
  }

  if (req.query.shipmentId) {
    const { error, data } = await getShipmentById(shipmentsData)(
      req.query.shipmentId,
    );

    if (error === errors.NOT_FOUND) {
      return res.status(404).json({ message: `Not Found!` });
    }

    return res.status(200).json(data);
  }

  const { error, data } = await getAllShipments(shipmentsData)();

  if (error === errors.NOT_FOUND) {
    return res.status(404).json({ message: `Not Found!` });
  }

  return res.status(200).json(data);
});

shipmentsRoute.post(
  '/',
  bodyValidator(shipmentsValidator),
  async (req, res) => {
    const { error, data } = await createShipments(shipmentsData)(req.body);
    if (error !== null) {
      return res.status(400).json(error);
    }

    if (error === errors.NOT_FOUND) {
      return res.status(400).json({
        message: `Failed to create`,
      });
    }

    return res.status(200).json(data);
  },
);

shipmentsRoute.put(
  '/:id',

  async (req, res) => {
    const { error, data } = await updateShipments(shipmentsData)(
      req.body,
      req.params.id,
    );

    if (error !== null) {
      return res.status(400).json(error);
    }

    if (error === errors.NOT_FOUND) {
      return res.status(404).json({
        message: `Not Found!`,
      });
    }

    return res.status(200).json(data);
  },
);

shipmentsRoute.delete(
  '/:id',
  bodyValidator(shipmentsValidator),
  async (req, res) => {
    const { error, data } = await deleteShipments(shipmentsData)(req.params.id);

    if (error === errors.NOT_FOUND) {
      return res.status(404).json({
        message: `Not Found!`,
      });
    }

    return res.status(200).json(data);
  },
);

export default shipmentsRoute;
