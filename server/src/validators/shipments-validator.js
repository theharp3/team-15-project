import constraints from './constraints.js';

export default {
  origin_warehouse: (value) => typeof value === 'string',
  destination_warehouse: (value) => typeof value === 'string',
  // departure_date: (value) => value === '',
  // arrival_date: (value) => value === '',
};
