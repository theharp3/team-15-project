export default {
  minFormEntry: 3,
  maxFormEntry: 45,
  maxParcelWeight: 100,
};
