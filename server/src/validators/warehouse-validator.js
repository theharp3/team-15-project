import constraints from './constraints.js';

export default {
  street_name: (value) => typeof value === 'string'
    && value.length >= constraints.minFormEntry
    && value.length <= constraints.maxFormEntry,
  cities_name: (value) => typeof value === 'string',
};
