import constraints from './constraints.js';

export default {
  categories_id: (value) => typeof value === 'number',
  weight: (value) => typeof value === 'number',
  users_id: (value) => typeof value === 'number',
  // to_customer_address: (value) => typeof value === 'number',
};
