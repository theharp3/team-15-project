/* eslint-disable import/extensions */
import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import dotenv from 'dotenv';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import usersRoute from './controllers/user-controller.js';
import locationsRoute from './controllers/locations-controller.js';
import warehousesRoute from './controllers/warehouse-controller.js';
import shipmentsRoute from './controllers/shipments-controller.js';
import parcelsRoute from './controllers/parcel-controller.js';
import categoriesRoute from './controllers/categories-controller.js';

const config = dotenv.config().parsed;

const app = express();

passport.use(jwtStrategy);
app.use(passport.initialize());

const PORT = +config.PORT;

app.use(helmet());
app.use(cors());
app.use(express.json());

app.use('/users', usersRoute);
app.use('/locations', locationsRoute);
app.use('/warehouses', warehousesRoute);
app.use('/shipments', shipmentsRoute);
app.use('/parcels', parcelsRoute);
app.use('/categories', categoriesRoute);

app.listen(process.env.PORT || PORT, () => console.log(`listening on port ${PORT}`));
