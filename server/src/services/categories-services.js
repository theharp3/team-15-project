import errors from "./errors.js";

const getAllCategories = (categoriesData) => async () => {
  const categories = await categoriesData.getAllCategories();
  if (categories === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: categories,
  };
};

export default getAllCategories;
