import bcrypt from 'bcryptjs';
import userRole from '../common/user-role.js';
import errors from './errors.js';

export const getUserByEmail = (usersData) => async (email) => {
  const user = await usersData.getUserByEmail(email);

  if (user === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: user,
  };
};
export const getUserByID = (usersData) => async (id) => {
  const user = await usersData.getUserByID(id);

  if (user === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: user,
  };
};

export const getAllCustomers = (usersData) => async () => await usersData.getAllCustomers();

export const validateUser = (usersData) => async ({ email, password }) => {
  const user = await getUserByEmail(usersData)(email);

  if (user.error) {
    throw new Error('email does not exist!');
  }

  if (await bcrypt.compare(password, user.data.password)) {
    return user.data;
  }

  return null;
};

export const createUser = (usersData) => async (user) => {
  const defaultRole = userRole.CUSTOMER;

  if (await usersData.userExists(user.email)) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }

  const createdUser = await usersData.createUser(user, defaultRole);

  return {
    error: null,
    data: createdUser,
  };
};
export const updateUser = (usersData) => async (changes, id) => {
  const createdUser = await usersData.updateUser(changes, id);
  if (!createdUser) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: createdUser,
  };
};

export const getUserByName = (usersData) => async (firstName, lastName) => {
  if (firstName && lastName) {
    const allUsers = await usersData.getUserByFullName(firstName, lastName);
    return {
      error: null,
      data: allUsers,
    };
  }
  if (firstName) {
    const allUsers = await usersData.getUserByFirstName(firstName);
    return {
      error: null,
      data: allUsers,
    };
  }

  const allUsers = await usersData.getUserByLastName(lastName);
  return {
    error: null,
    data: allUsers,
  };
};
export const getUserBySearchTerm = (usersData) => async (search) => {
  const users = await usersData.getUserBySearchTerm(search);

  if (users === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: users,
  };
};

export const checkPass = (usersData) => async (password) => {
  const check = await usersData.checkPass(password);
  if (check === null) {
    return {
      error: errors.INPUT_ERROR,
    };
  }
  return {
    error: null,
  };
};
