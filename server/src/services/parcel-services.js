import errors from './errors.js';

export const getAllParcels = (parcelsData) => async () => {
  const allParcels = await parcelsData.getAllParcels();
  if (allParcels === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: allParcels,
  };
};

export const getParcelById = (parcelsData) => async (parcelId) => {
  const parcel = await parcelsData.getParcelById(parcelId);
  if (parcel === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: parcel,
  };
};

export const createParcel = (parcelData) => async (bodyData) => {
  const createdParcel = await parcelData.createParcel(bodyData);

  if (createdParcel === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: createdParcel,
  };
};

export const updateParcel = (parcelData) => async (bodyData, parcelId) => {
  const updatedParcel = await parcelData.updateParcel(bodyData, parcelId);

  if (updatedParcel === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: updatedParcel,
  };
};

export const deleteParcel = (parcelData) => async (parcelId) => {
  const deletedParcel = await parcelData.deleteParcel(parcelId);

  if (deletedParcel === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: deletedParcel,
  };
};

export const filterAndSortParcels =
  (parcelData) => async (filterCriteria, sortingCriteria) => {
    const filteredParcels = await parcelData.filterAndSortParcels(
      filterCriteria,
      sortingCriteria,
    );

    if (filteredParcels === null) {
      return {
        error: errors.NOT_FOUND,
        data: null,
      };
    }

    return {
      error: null,
      data: filteredParcels,
    };
  };

export const getAllParcelsCards = (parcelsData) => async () => {
  const allParcels = await parcelsData.getAllParcelsCardsData();
  if (allParcels === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: allParcels,
  };
};

export const getAllParcelsCategories = (parcelsData) => async () => {
  const allParcels = await parcelsData.getAllParcelsCategoriesData();
  if (allParcels === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: allParcels,
  };
};

export const getAllParcelsByShipment = (parcelsData) => async (shipmentId) => {
  const allParcels = await parcelsData.getAllParcelsByShipmentData(shipmentId);
  if (allParcels === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: allParcels,
  };
};
