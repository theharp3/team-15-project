import errors from './errors.js';

export const getAllCountries = (locationsData) => async () => {
  const countriesData = await locationsData.getAllCountriesData();
  if (countriesData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: countriesData,
  };
};

export const getAllCities = (locationsData) => async () => {
  const citiesData = await locationsData.getAllCitiesData();
  if (citiesData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: citiesData,
  };
};

export const getCitiesFromCountry = (locationsData) => async (country) => {
  const citiesData = await locationsData.getCitiesFromCountry(country);
  if (citiesData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: citiesData,
  };
};
