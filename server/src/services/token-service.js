export const blacklistToken = (tokenData) => async (token) => {
  await tokenData.blacklistToken(token);
};

export const tokenExists = (tokenData) => async (token) => await tokenData.tokenExists(token);
