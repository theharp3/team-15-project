import errors from './errors.js';

export const getAllWarehouses = (warehouseData) => async () => {
  const whData = await warehouseData.getAllWarehousesData();
  if (whData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: whData,
  };
};

export const getWarehouseById = (warehouseData) => async (id) => {
  const whData = await warehouseData.getWarehouseByIdData(id);
  if (whData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: whData,
  };
};

export const createWarehouses = (warehouseData) => async (bodyData) => {
  const whData = await warehouseData.createWarehousesData(bodyData);

  if (whData === null) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }

  return {
    error: null,
    data: whData,
  };
};

export const updateWarehouses =
  (warehouseData) => async (bodyData, warehouseID) => {
    const whData = await warehouseData.updateWarehousesData(
      bodyData,
      warehouseID,
    );

    if (whData === null) {
      return {
        error: errors.DUPLICATE_RECORD,
        data: null,
      };
    }

    return {
      error: null,
      data: whData,
    };
  };

export const deleteWarehouses = (warehouseData) => async (warehouseID) => {
  const whData = await warehouseData.deleteWarehousesData(warehouseID);

  if (whData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: whData,
  };
};

export const filterWarehouse = (warehouseData) => async (filterCriteria) => {
  const filteredWarehouses = await warehouseData.filterWarehouse(filterCriteria);

  if (filteredWarehouses === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: filteredWarehouses,
  };
};