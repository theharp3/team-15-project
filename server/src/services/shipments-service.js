import errors from './errors.js';

export const getAllShipments = (shipmentsData) => async () => {
  const spmData = await shipmentsData.getAllShipmentsData();
  if (spmData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: spmData,
  };
};

export const getAllShipmentsWithStatus = (shipmentsData) => async (status) => {
  const spmData = await shipmentsData.getAllShipmentsWithStatusData(status);
  if (spmData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: spmData,
  };
};

export const getShipmentById = (shipmentsData) => async (shipmentId) => {
  const spmData = await shipmentsData.getShipmentByIdData(shipmentId);
  if (spmData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: spmData,
  };
};

export const getInboundShipments = (shipmentsData) => async (warehouseID) => {
  const spmData = await shipmentsData.getInboundShipmentsData(warehouseID);
  if (spmData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: spmData,
  };
};

export const getAllShipmentsByWarehouseId =
  (shipmentsData) => async (warehouseID) => {
    const spmData = await shipmentsData.getAllShipmentsByWarehouseData(
      warehouseID,
    );
    if (spmData === null) {
      return {
        error: errors.NOT_FOUND,
        data: null,
      };
    }
    return {
      error: null,
      data: spmData,
    };
  };

export const getAllShipmentsByCustomerId =
  (warehouseData) => async (customerId) => {
    const spmData = await warehouseData.getAllShipmentsByCustomerData(
      customerId,
    );
    if (spmData === null) {
      return {
        error: errors.NOT_FOUND,
        data: null,
      };
    }
    return {
      error: null,
      data: spmData,
    };
  };

export const createShipments = (shipmentsData) => async (bodyData) => {
  if (bodyData.destination_warehouse === bodyData.origin_warehouse) {
    return {
      error: { message: "Destination can't be same as origin!" },
      data: null,
    };
  }

  if (
    bodyData.arrival_date < bodyData.departure_date &&
    bodyData.arrival_date !== ''
  ) {
    return {
      error: { message: "Arrival date can't be before departure date!" },
      data: null,
    };
  }

  const spmData = await shipmentsData.createShipmentsData(bodyData);

  if (spmData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: spmData,
  };
};

export const updateShipments =
  (shipmentsData) => async (bodyData, shipmentId) => {
    if (
      bodyData.destination_warehouse &&
      bodyData.destination_warehouse === bodyData.origin_warehouse
    ) {
      return {
        error: { message: "Destination can't be same as origin!" },
        data: null,
      };
    }

    if (
      bodyData.arrival_date < bodyData.departure_date &&
      bodyData.arrival_date !== ''
    ) {
      return {
        error: { message: "Arrival date can't be before departure date!" },
        data: null,
      };
    }

    const spmData = await shipmentsData.updateShipmentsData(
      bodyData,
      shipmentId,
    );

    if (spmData === null) {
      return {
        error: errors.NOT_FOUND,
        data: null,
      };
    }

    return {
      error: null,
      data: spmData,
    };
  };

export const deleteShipments = (shipmentsData) => async (shipmentId) => {
  const spmData = await shipmentsData.deleteShipmentsData(shipmentId);

  if (spmData === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: spmData,
  };
};
