import pool from './pool.js';

export const getAllParcels = async () => {
  const selectAllParcels = `SELECT * FROM parcels;`;
  const parcels = await pool.query(selectAllParcels);

  if (parcels.length === 0) {
    return null;
  }

  return parcels;
};
export const getParcelById = async (parcelId) => {
  const selectParcelsById = `SELECT users.first_name,warehouses.street_name,categories.name,parcels.*, users.last_name
  FROM parcels
  JOIN categories ON parcels.categories_id = categories.id
  LEFT JOIN warehouses ON warehouses_origin = warehouses.id
  JOIN users ON users_id = users.id
  WHERE parcels.id = ?;`;
  const selectParcelsRes = await pool.query(selectParcelsById, parcelId);

  if (selectParcelsRes.length === 0) {
    return null;
  }

  return selectParcelsRes[0];
};

export const createParcel = async (bodyData) => {
  const { users_id, weight, categories_id } = bodyData;
  const insertParcel = `INSERT INTO parcels (users_id, weight, categories_id)
  VALUES (?,?,?);`;
  const res = await pool.query(insertParcel, [users_id, weight, categories_id]);
  const bodyArr = Object.entries(bodyData);
  bodyArr.forEach(async (el) => {
    if (el[1] !== null) {
      await pool.query(
        `UPDATE parcels SET ${el[0]} = ${el[1]} WHERE (id = ${res.insertId})`,
      );
    }
  });
  const selectRes = `SELECT * FROM parcels WHERE id=?;`;
  const createdParcel = await pool.query(selectRes, res.insertId);
  if (createdParcel.length === 0) {
    return null;
  }

  return createdParcel[0];
};

export const updateParcel = async (bodyData, parcelId) => {
  const selectParcel = `SELECT * FROM parcels WHERE id=?;`;
  const check = await pool.query(selectParcel, parcelId);
  if (check.length === 0) {
    return null;
  }

  const bodyArr = Object.entries(bodyData);
  bodyArr.forEach(async (el) => {
    if (el[1] !== null) {
      await pool.query(
        `UPDATE parcels SET ${el[0]} = "${el[1]}" WHERE (id = ${parcelId})`,
      );
    } else {
      await pool.query(
        `UPDATE parcels SET ${el[0]} = ${el[1]} WHERE (id = ${parcelId})`,
      );
    }
  });
  const updatedParcel = await pool.query(selectParcel, parcelId);
  if (updatedParcel[0]) {
    return updatedParcel[0];
  }

  return null;
};

export const deleteParcel = async (parcelId) => {
  const select = `SELECT * FROM parcels WHERE id = ?;`;
  const check = await pool.query(select, parcelId);
  if (!check[0]) {
    return null;
  }

  const parcelDelete = `DELETE FROM parcels WHERE id=?;`;
  const deletedParcel = await pool.query(parcelDelete, parcelId);

  return deletedParcel;
};

export const filterAndSortParcels = async (filterCriteria, sortingCriteria) => {
  const select = `SELECT users.first_name,warehouses.street_name,categories.name,parcels.* FROM parcels
  JOIN categories ON parcels.categories_id = categories.id
  LEFT JOIN warehouses ON warehouses_origin = warehouses.id
  JOIN users ON users_id = users.id
  ${filterCriteria.length !== 0 ? `WHERE ${filterCriteria.join(' AND ')}` : ''}
  ${sortingCriteria.length !== 0 ? `ORDER BY ${sortingCriteria.join(',')}` : ''}
`;
  let parcels;
  try {
    parcels = await pool.query(select);
  } catch (error) {
    return null;
  }

  if (parcels.length === 0) {
    return null;
  }
  return parcels;
};

export const getAllParcelsCardsData = async () => {
  const selectAllParcels = `select parcels.*, categories.name as category, users.first_name, users.last_name, users.email, users.user_city, users.user_country, users.address
  from parcels
  join categories on parcels.categories_id = categories.id
  join users on parcels.users_id = users.id;`;
  const parcels = await pool.query(selectAllParcels);
  if (parcels.length === 0) {
    return null;
  }

  return parcels;
};

export const getAllParcelsCategoriesData = async () => {
  const selectAllParcels = `select * from categories`;
  const parcels = await pool.query(selectAllParcels);
  if (parcels.length === 0) {
    return null;
  }

  return parcels;
};

export const getAllParcelsByShipmentData = async (shipmentId) => {
  const selectAllParcels = `select parcels.*, categories.name as category
  from parcels
  join categories on parcels.categories_id = categories.id
  where shipments_id = ?`;
  const parcels = await pool.query(selectAllParcels, [shipmentId]);
  if (parcels.length === 0) {
    return null;
  }

  return parcels;
};
