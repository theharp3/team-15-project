import pool from './pool.js';

export const getAllCountriesData = async () => {
  const selectAllCountries = `SELECT * FROM countries;`;
  const res = await pool.query(selectAllCountries);

  if (res.length === 0) {
    return null;
  }

  return res;
};

export const getAllCitiesData = async () => {
  const selectAllCities = `SELECT * FROM cities;`;
  const res = await pool.query(selectAllCities);

  if (res.length === 0) {
    return null;
  }

  return res;
};

export const getCitiesFromCountry = async (country) => {
  const selectAllCities = `
  SELECT cities.*,countries.countries_name 
  FROM cities
  LEFT JOIN countries ON countries.id = cities.countries_id
  WHERE countries.countries_name = '${country}';`;
  const res = await pool.query(selectAllCities);

  if (res.length === 0) {
    return null;
  }

  return res;
};
