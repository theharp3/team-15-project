import pool from './pool.js';

export const getAllWarehousesData = async () => {
  const selectAllWarehouses = `SELECT warehouses.id, warehouses.street_name, cities.cities_name, countries.countries_name
  FROM warehouses
  JOIN cities ON warehouses.cities_id = cities.id
  JOIN countries ON cities.countries_id = countries.id;`;
  const res = await pool.query(selectAllWarehouses);
  if (res.length === 0) {
    return null;
  }

  return res;
};

export const getWarehouseByIdData = async (id) => {
  const selectAllWarehouses = `SELECT warehouses.id, warehouses.street_name, cities.cities_name, countries.countries_name
  FROM warehouses
  JOIN cities ON warehouses.cities_id = cities.id
  JOIN countries ON cities.countries_id = countries.id
  WHERE warehouses.id = ?`;
  const res = await pool.query(selectAllWarehouses, [id]);
  if (res.length === 0) {
    return null;
  }

  return res[0];
};

export const createWarehousesData = async (bodyData) => {
  const { street_name, cities_name } = bodyData;

  const select = `SELECT * FROM warehouses WHERE street_name=?;`;
  const duplicateCheck = await pool.query(select, [street_name]);
  if (!(typeof duplicateCheck[0] === 'undefined')) {
    return null;
  }

  const selectCityId = `SELECT id FROM cities WHERE cities_name=?;`;
  const cityId = await pool.query(selectCityId, [cities_name]);

  const insertWarehouse = `INSERT INTO warehouses (street_name, cities_id)
  VALUES (?, ?);`;
  const res = await pool.query(insertWarehouse, [street_name, cityId[0].id]);

  const selectRes = `SELECT * FROM warehouses WHERE id=?;`;
  const createdWarehouseData = await pool.query(selectRes, [res.insertId]);
  if (createdWarehouseData.length === 0) {
    return null;
  }

  return createdWarehouseData[0];
};

export const updateWarehousesData = async (bodyData, warehouseID) => {
  const { street_name, cities_name } = bodyData;

  // const selectAllWarehouseStreetNames = `SELECT street_name FROM warehouses;`;
  // const namesArr = await pool.query(selectAllWarehouseStreetNames);
  // console.log(namesArr);
  // const isNameInNamesArr = namesArr.some((e) => e.street_name === street_name);
  // if (isNameInNamesArr) {
  //   return null;
  // }

  const selectCityId = `SELECT id FROM cities WHERE cities_name=?;`;
  const cityId = await pool.query(selectCityId, [cities_name]);

  const updateWarehouse = `UPDATE warehouses SET street_name = ?, cities_id = ?
  WHERE (id = ?);`;
  const res = await pool.query(updateWarehouse, [
    street_name,
    cityId[0].id,
    warehouseID,
  ]);

  const selectRes = `SELECT * FROM warehouses WHERE id=?;`;
  const updatedWarehouseData = await pool.query(selectRes, [warehouseID]);
  if (updatedWarehouseData.length === 0) {
    return null;
  }

  return updatedWarehouseData[0];
};

export const deleteWarehousesData = async (warehouseID) => {
  const select = `SELECT * FROM warehouses WHERE id = ?;`;
  const existsCheck = await pool.query(select, [warehouseID]);
  if (typeof existsCheck[0] === 'undefined') {
    return null;
  }

  const deleteWarehouse = `DELETE FROM warehouses WHERE id=?;`;
  const res = await pool.query(deleteWarehouse, [warehouseID]);

  return res;
};

export const filterWarehouse = async (filterCriteria) => {
  const select = `SELECT warehouses.id, warehouses.street_name, cities.cities_name, countries.countries_name
  FROM warehouses
  JOIN cities ON warehouses.cities_id = cities.id
  JOIN countries ON cities.countries_id = countries.id
  ${filterCriteria.length !== 0 ? `WHERE ${filterCriteria.join(' AND ')}` : ''}
`;

  let warehouses;
  try {
    warehouses = await pool.query(select);
  } catch (error) {
    return null;
  }

  if (warehouses.length === 0) {
    return null;
  }
  return warehouses;
};
