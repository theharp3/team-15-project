import bcrypt from 'bcryptjs';
import pool from './pool.js';

export const getUserByEmail = async (email) => {
  const res = await pool.query(
    `SELECT roles.role, users.*
  FROM users
  JOIN roles ON users.roles_id = roles.id
  WHERE users.email LIKE '%${email}%'`,
  );

  if (res.length === 0) {
    return null;
  }

  return res[0];
};
export const getUserByFullName = async (firstName, lastName) => {
  const res = await pool.query(
    `SELECT roles.role, users.*
  FROM users
  JOIN roles ON users.roles_id = roles.id
  WHERE users.first_name LIKE '%${firstName}%' AND users.last_name LIKE '%${lastName}%'`,
  );

  if (res.length === 0) {
    return null;
  }

  return res[0];
};
export const getUserByFirstName = async (firstName) => {
  const res = await pool.query(
    `SELECT roles.role, users.*
  FROM users
  JOIN roles ON users.roles_id = roles.id
  WHERE users.first_name LIKE '%${firstName}%'`,
  );

  if (res.length === 0) {
    return null;
  }

  return res[0];
};
export const getUserByLastName = async (lastName) => {
  const res = await pool.query(
    `SELECT roles.role, users.*
  FROM users
  JOIN roles ON users.roles_id = roles.id
  WHERE users.last_name LIKE '%${lastName}%'`,
  );

  if (res.length === 0) {
    return null;
  }

  return res[0];
};
export const getUserBySearchTerm = async (search) => {
  const res = await pool.query(
    `SELECT roles.role, users.*
  FROM users
  JOIN roles ON users.roles_id = roles.id
  WHERE users.last_name LIKE '%${search}%' OR users.first_name LIKE '%${search}%' OR users.email LIKE '%${search}%'`,
  );

  if (res.length === 0) {
    return null;
  }

  return res[0];
};

export const userExists = async (email) => !!(await getUserByEmail(email));

export const checkPass = async (password) => {
  const hashPassword = await bcrypt.hash(password, 10);
  console.log(hashPassword);

  const res = await pool.query(
    `SELECT users.*
  FROM users
  WHERE users.password = '${hashPassword}'`,
  );

  if (res.length === 0) {
    return null;
  }

  return res[0];
};

export const getUserByID = async (id) => {
  const res = await pool.query('select * from users u where u.id = ?', [id]);

  if (res.length === 0) {
    return null;
  }

  return res[0];
};

export const createUser = async (user) => {
  user.password = await bcrypt.hash(user.password, 10);

  const sql = `
    insert into users (first_name, last_name, email, password)
    values (?, ?, ?, ?)
  `;

  const result = await pool.query(sql, [
    user.first_name,
    user.last_name,
    user.email,
    user.password,
  ]);

  const sql2 = `
    select *
    from users u
    where u.id = ?
  `;

  const createdUser = (await pool.query(sql2, [result.insertId]))[0];

  return createdUser;
};

export const updateUser = async (changes, id) => {
  if (changes.password) {
    changes.password = await bcrypt.hash(changes.password, 10);
  }
  const updateParams = Object.entries(changes).map(
    ([key, value]) => `${key} = "${value}"`,
  );
  const update = `UPDATE users 
  SET ${updateParams.length !== 0 ? `${updateParams.join(',')}` : ''} 
  WHERE id = ${id}`;
  const select = `SELECT users.id, users.first_name, users.last_name, users.email, users.createdOn, 
  users.user_city, users.user_country, users.address, users.post_code, roles.role FROM users
  JOIN roles ON roles.id = users.roles_id
  WHERE users.id = ${id}`;
  try {
    await pool.query(update);
  } catch (error) {
    return null;
  }
  const updatedUser = await pool.query(select);
  return updatedUser[0];
};

export const getAllCustomers = async () => await pool.query(`
    select u.id, u.first_name, u.last_name, u.email, u.createdOn
    from users u
    WHERE u.roles_id = 1
  `);
export const getAllUserParcels = async (id) => await pool.query(`
  select * from parcels where users_id = ${id} AND due < ${new Date()}
`);
