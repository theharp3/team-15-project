import pool from "./pool.js";

export const getAllCategories = async () => {
  const selectAllCategories = `SELECT * FROM categories;`;
  const res = await pool.query(selectAllCategories);

  if (res.length === 0) {
    return null;
  }

  return res;
};
