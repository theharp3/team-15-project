import pool from './pool.js';

export const getAllShipmentsData = async () => {
  const selectAllShipments = `select shipments.id, shipments.departure_date, shipments.arrival_date, shipments.total_cargo_weight, shipment_status.status, wh_origin.street_name as origin_warehouse, wh_destination.street_name as destination_warehouse
  from shipments
  join shipment_status on shipments.shipment_status_id = shipment_status.id
  join warehouses as wh_origin on shipments.origin_id = wh_origin.id 
  join warehouses as wh_destination on shipments.destination_id = wh_destination.id;`;
  const res = await pool.query(selectAllShipments);

  if (res.length === 0) {
    return null;
  }

  return res;
};

export const getShipmentByIdData = async (shipmentsId) => {
  const selectAllShipments = `select shipments.id, shipments.departure_date, shipments.arrival_date, shipments.total_cargo_weight, shipment_status.status, wh_origin.street_name as origin_warehouse, wh_destination.street_name as destination_warehouse
  from shipments
  join shipment_status on shipments.shipment_status_id = shipment_status.id
  join warehouses as wh_origin on shipments.origin_id = wh_origin.id 
  join warehouses as wh_destination on shipments.destination_id = wh_destination.id
  where shipments.id = ?;`;
  const res = await pool.query(selectAllShipments, [shipmentsId]);

  if (res.length === 0) {
    return null;
  }

  return res[0];
};

export const getAllShipmentsWithStatusData = async (status) => {
  const selectAllShipments = `select * from shipments where shipment_status_id = ?;`;
  const res = await pool.query(selectAllShipments, [status]);

  if (res.length === 0) {
    return null;
  }

  return res;
};

export const getAllShipmentsByWarehouseData = async (warehouseID) => {
  const selectAllShipments = `SELECT * FROM shipments WHERE origin_id = ?;`;
  const res = await pool.query(selectAllShipments, [warehouseID]);

  if (res.length === 0) {
    return null;
  }

  return res;
};

export const getInboundShipmentsData = async (warehouseID) => {
  const selectAllShipments = `select shipments.id, shipments.arrival_date, wh_origin.street_name as origin_warehouse
  from shipments
  join warehouses as wh_origin on shipments.origin_id = wh_origin.id 
  WHERE destination_id = ? AND shipment_status_id = 2;`;
  const res = await pool.query(selectAllShipments, [warehouseID]);

  if (res.length === 0) {
    return null;
  }

  return res;
};

export const getAllShipmentsByCustomerData = async (customerId) => {
  const selectParcels = `SELECT * FROM parcels WHERE customers_id = ?;`;
  const selectParcelsRes = await pool.query(selectParcels, [customerId]);

  const res = await Promise.all(
    selectParcelsRes.map(async (e) => {
      const element = await pool.query(
        `SELECT * FROM shipments WHERE id = ${e['shipments_id']};`,
      );

      return element[0];
    }),
  );

  if (res.length === 0) {
    return null;
  }

  return res;
};

export const createShipmentsData = async (bodyData) => {
  const { origin_warehouse, destination_warehouse } = bodyData;

  const selectOriginWarehouseId = `SELECT id FROM warehouses WHERE street_name=?;`;
  const originWarehouseId = await pool.query(selectOriginWarehouseId, [
    origin_warehouse,
  ]);

  const selectDestinationWarehouseId = `SELECT id FROM warehouses WHERE street_name=?;`;
  const destinationWarehouseId = await pool.query(
    selectDestinationWarehouseId,
    [destination_warehouse],
  );

  const insertShipments = `INSERT INTO shipments (origin_id, destination_id)
  VALUES (?, ?);`;
  const res = await pool.query(insertShipments, [
    originWarehouseId[0].id,
    destinationWarehouseId[0].id,
  ]);

  const bodyArr = Object.entries(bodyData)
    .filter(
      (e) =>
        e[0] !== 'destination_warehouse' &&
        e[0] !== 'origin_warehouse' &&
        e[1] !== null &&
        e[1] !== '',
    )
    .forEach(async (e) => {
      await pool.query(
        `UPDATE shipments SET ${e[0]} = '${e[1]}' WHERE (id = ${res.insertId})`,
      );
    });

  const selectRes = `SELECT * FROM shipments WHERE id=?;`;
  const createdShipmentsData = await pool.query(selectRes, [res.insertId]);
  if (createdShipmentsData.length === 0) {
    return null;
  }

  return createdShipmentsData[0];
};

export const updateShipmentsData = async (bodyData, shipmentId) => {
  const select = `SELECT * FROM shipments WHERE id=?;`;
  const checkIfExists = await pool.query(select, [shipmentId]);
  if (checkIfExists.length === 0) {
    return null;
  }
  // need to set warehouses manually
  const bodyArr = Object.entries(bodyData);
  bodyArr.forEach(async (e) => {
    if (e[1] !== null && e[1] !== '') {
      await pool.query(
        `UPDATE shipments SET ${e[0]} = '${e[1]}' WHERE (id = ${shipmentId})`,
      );
    }
  });

  const selectRes = `SELECT * FROM shipments WHERE id=?;`;
  const updatedShipmentData = await pool.query(selectRes, [shipmentId]);
  if (updatedShipmentData.length === 0) {
    return null;
  }

  return updatedShipmentData[0];
};

export const deleteShipmentsData = async (shipmentId) => {
  const select = `SELECT * FROM shipments WHERE id = ?;`;
  const existsCheck = await pool.query(select, [shipmentId]);
  if (typeof existsCheck[0] === 'undefined') {
    return null;
  }

  const deleteShipment = `DELETE FROM shipments WHERE id=?;`;
  const res = await pool.query(deleteShipment, [shipmentId]);

  return res;
};
