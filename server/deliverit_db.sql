-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema deliverit_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema deliverit_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `deliverit_db` DEFAULT CHARACTER SET utf8mb3 ;
USE `deliverit_db` ;

-- -----------------------------------------------------
-- Table `deliverit_db`.`categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverit_db`.`categories` ;

CREATE TABLE IF NOT EXISTS `deliverit_db`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliverit_db`.`countries`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverit_db`.`countries` ;

CREATE TABLE IF NOT EXISTS `deliverit_db`.`countries` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliverit_db`.`cities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverit_db`.`cities` ;

CREATE TABLE IF NOT EXISTS `deliverit_db`.`cities` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `countries_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cities_countries1_idx` (`countries_id` ASC) VISIBLE,
  CONSTRAINT `fk_cities_countries1`
    FOREIGN KEY (`countries_id`)
    REFERENCES `deliverit_db`.`countries` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliverit_db`.`shipment_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverit_db`.`shipment_status` ;

CREATE TABLE IF NOT EXISTS `deliverit_db`.`shipment_status` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliverit_db`.`warehouses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverit_db`.`warehouses` ;

CREATE TABLE IF NOT EXISTS `deliverit_db`.`warehouses` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `street_name` VARCHAR(45) NOT NULL,
  `cities_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_warehouses_cities_idx` (`cities_id` ASC) VISIBLE,
  CONSTRAINT `fk_warehouses_cities`
    FOREIGN KEY (`cities_id`)
    REFERENCES `deliverit_db`.`cities` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliverit_db`.`shipments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverit_db`.`shipments` ;

CREATE TABLE IF NOT EXISTS `deliverit_db`.`shipments` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `origin_id` INT(11) NOT NULL,
  `departure_date` DATETIME NULL DEFAULT NULL,
  `arrival_date` DATETIME NULL DEFAULT NULL,
  `destination_id` INT(11) NULL DEFAULT NULL,
  `total_cargo_weight` INT(11) NULL DEFAULT NULL,
  `shipment_status_id` INT(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_shipments_warehouses1_idx` (`origin_id` ASC) VISIBLE,
  INDEX `fk_shipments_warehouses2_idx` (`destination_id` ASC) VISIBLE,
  INDEX `fk_shipments_shipment_status1_idx` (`shipment_status_id` ASC) VISIBLE,
  CONSTRAINT `fk_shipments_shipment_status1`
    FOREIGN KEY (`shipment_status_id`)
    REFERENCES `deliverit_db`.`shipment_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipments_warehouses1`
    FOREIGN KEY (`origin_id`)
    REFERENCES `deliverit_db`.`warehouses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipments_warehouses2`
    FOREIGN KEY (`destination_id`)
    REFERENCES `deliverit_db`.`warehouses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliverit_db`.`roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverit_db`.`roles` ;

CREATE TABLE IF NOT EXISTS `deliverit_db`.`roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliverit_db`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverit_db`.`users` ;

CREATE TABLE IF NOT EXISTS `deliverit_db`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `password` VARCHAR(255) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `cities_id` INT(11) DEFAULT NULL,
  `roles_id` INT(11) NULL DEFAULT 1,
  `createdOn` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
  `avatar` VARCHAR(45) NULL DEFAULT NULL,
  `address` VARCHAR(45) NULL DEFAULT NULL,
  `ban_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_cities1_idx` (`cities_id` ASC) VISIBLE,
  INDEX `fk_users_roles1_idx` (`roles_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_cities1`
    FOREIGN KEY (`cities_id`)
    REFERENCES `deliverit_db`.`cities` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_roles1`
    FOREIGN KEY (`roles_id`)
    REFERENCES `deliverit_db`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliverit_db`.`parcels`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverit_db`.`parcels` ;

CREATE TABLE IF NOT EXISTS `deliverit_db`.`parcels` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `categories_id` INT(11) NOT NULL,
  `weight` INT(3) NOT NULL,
  `warehouses_todeliver` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `warehouses_origin` INT(11) NOT NULL,
  `shipments_id` INT(11) NULL DEFAULT NULL,
  `deliver_to_customer_address` INT(3) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_parcel_categories1_idx` (`categories_id` ASC) VISIBLE,
  INDEX `fk_parcel_warehouses1_idx` (`warehouses_todeliver` ASC) VISIBLE,
  INDEX `fk_parcel_shipments1_idx` (`shipments_id` ASC) VISIBLE,
  INDEX `fk_parcels_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_parcels_warehouses1_idx` (`warehouses_origin` ASC) VISIBLE,
  CONSTRAINT `fk_parcel_categories1`
    FOREIGN KEY (`categories_id`)
    REFERENCES `deliverit_db`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcel_shipments1`
    FOREIGN KEY (`shipments_id`)
    REFERENCES `deliverit_db`.`shipments` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcel_warehouses1`
    FOREIGN KEY (`warehouses_todeliver`)
    REFERENCES `deliverit_db`.`warehouses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcels_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `deliverit_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcels_warehouses1`
    FOREIGN KEY (`warehouses_origin`)
    REFERENCES `deliverit_db`.`warehouses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliverit_db`.`tokens`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverit_db`.`tokens` ;

CREATE TABLE IF NOT EXISTS `deliverit_db`.`tokens` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
