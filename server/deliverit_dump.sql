-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.2-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for deliverit_db
DROP DATABASE IF EXISTS `deliverit_db`;
CREATE DATABASE IF NOT EXISTS `deliverit_db` /*!40100 DEFAULT CHARACTER SET utf8mb3 */;
USE `deliverit_db`;

-- Dumping structure for table deliverit_db.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table deliverit_db.categories: ~4 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
REPLACE INTO `categories` (`id`, `name`) VALUES
	(1, 'Devices'),
	(2, 'Clothing'),
	(3, 'Books'),
	(4, 'Furniture');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table deliverit_db.cities
DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cities_name` varchar(45) NOT NULL,
  `countries_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cities_countries1_idx` (`countries_id`),
  CONSTRAINT `fk_cities_countries1` FOREIGN KEY (`countries_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table deliverit_db.cities: ~7 rows (approximately)
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
REPLACE INTO `cities` (`id`, `cities_name`, `countries_id`) VALUES
	(1, 'Sofia', 1),
	(2, 'Plovdiv', 1),
	(3, 'Tokyo', 3),
	(4, 'Paris', 5),
	(5, 'New York', 4),
	(6, 'Berlin', 6),
	(7, 'London', 2);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;

-- Dumping structure for table deliverit_db.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `countries_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table deliverit_db.countries: ~6 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
REPLACE INTO `countries` (`id`, `countries_name`) VALUES
	(1, 'Bulgaria'),
	(2, 'UK'),
	(3, 'Japan'),
	(4, 'USA'),
	(5, 'France'),
	(6, 'Germany');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table deliverit_db.parcels
DROP TABLE IF EXISTS `parcels`;
CREATE TABLE IF NOT EXISTS `parcels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categories_id` int(11) NOT NULL,
  `weight` int(3) NOT NULL,
  `warehouses_todeliver` int(11) DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `warehouses_origin` int(11) DEFAULT NULL,
  `shipments_id` int(11) DEFAULT NULL,
  `deliver_to_customer_address` int(3) DEFAULT 0,
  `due` datetime DEFAULT NULL,
  `public_id` varchar(45) DEFAULT '120',
  `state_of_delivery` varchar(45) DEFAULT 'waiting',
  PRIMARY KEY (`id`),
  KEY `fk_parcel_categories1_idx` (`categories_id`),
  KEY `fk_parcel_warehouses1_idx` (`warehouses_todeliver`),
  KEY `fk_parcel_shipments1_idx` (`shipments_id`),
  KEY `fk_parcels_users1_idx` (`users_id`),
  KEY `fk_parcels_warehouses1_idx` (`warehouses_origin`),
  CONSTRAINT `fk_parcel_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcel_shipments1` FOREIGN KEY (`shipments_id`) REFERENCES `shipments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcel_warehouses1` FOREIGN KEY (`warehouses_todeliver`) REFERENCES `warehouses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcels_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcels_warehouses1` FOREIGN KEY (`warehouses_origin`) REFERENCES `warehouses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table deliverit_db.parcels: ~6 rows (approximately)
/*!40000 ALTER TABLE `parcels` DISABLE KEYS */;
REPLACE INTO `parcels` (`id`, `categories_id`, `weight`, `warehouses_todeliver`, `users_id`, `warehouses_origin`, `shipments_id`, `deliver_to_customer_address`, `due`, `public_id`, `state_of_delivery`) VALUES
	(1, 1, 1, 1, 5, 2, NULL, 0, NULL, '120', 'waiting'),
	(2, 4, 56, NULL, 7, NULL, NULL, 0, NULL, '120', 'waiting'),
	(3, 3, 5, NULL, 3, NULL, NULL, 0, NULL, '120', 'waiting'),
	(4, 2, 23, NULL, 8, NULL, NULL, 0, NULL, '120', 'waiting'),
	(5, 3, 2, NULL, 10, NULL, NULL, 0, NULL, '120', 'waiting'),
	(6, 1, 2, NULL, 11, NULL, NULL, 0, NULL, '120', 'waiting');
/*!40000 ALTER TABLE `parcels` ENABLE KEYS */;

-- Dumping structure for table deliverit_db.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table deliverit_db.roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
REPLACE INTO `roles` (`id`, `role`) VALUES
	(1, 'Customer'),
	(2, 'Employee'),
	(3, 'Admin');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table deliverit_db.shipments
DROP TABLE IF EXISTS `shipments`;
CREATE TABLE IF NOT EXISTS `shipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin_id` int(11) NOT NULL,
  `departure_date` datetime DEFAULT NULL,
  `arrival_date` datetime DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  `total_cargo_weight` int(11) DEFAULT 0,
  `shipment_status_id` int(11) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `fk_shipments_warehouses1_idx` (`origin_id`),
  KEY `fk_shipments_warehouses2_idx` (`destination_id`),
  KEY `fk_shipments_shipment_status1_idx` (`shipment_status_id`),
  CONSTRAINT `fk_shipments_shipment_status1` FOREIGN KEY (`shipment_status_id`) REFERENCES `shipment_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipments_warehouses1` FOREIGN KEY (`origin_id`) REFERENCES `warehouses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipments_warehouses2` FOREIGN KEY (`destination_id`) REFERENCES `warehouses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table deliverit_db.shipments: ~5 rows (approximately)
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
REPLACE INTO `shipments` (`id`, `origin_id`, `departure_date`, `arrival_date`, `destination_id`, `total_cargo_weight`, `shipment_status_id`) VALUES
	(1, 1, '2021-08-26 00:00:00', '2021-10-29 00:00:00', 2, 0, 1),
	(2, 2, '2021-08-23 19:21:20', '2021-09-30 00:00:00', 3, 0, 1),
	(3, 3, '2021-08-24 21:20:24', '2021-08-26 19:20:47', 1, 0, 2),
	(4, 5, '2021-08-27 00:00:00', '2021-09-23 00:00:00', 4, 0, 1),
	(5, 4, '2021-08-30 00:00:00', '2021-09-16 00:00:00', 1, 0, 2);
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;

-- Dumping structure for table deliverit_db.shipment_status
DROP TABLE IF EXISTS `shipment_status`;
CREATE TABLE IF NOT EXISTS `shipment_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table deliverit_db.shipment_status: ~3 rows (approximately)
/*!40000 ALTER TABLE `shipment_status` DISABLE KEYS */;
REPLACE INTO `shipment_status` (`id`, `status`) VALUES
	(1, 'Preparing'),
	(2, 'En route'),
	(3, 'Delivered');
/*!40000 ALTER TABLE `shipment_status` ENABLE KEYS */;

-- Dumping structure for table deliverit_db.tokens
DROP TABLE IF EXISTS `tokens`;
CREATE TABLE IF NOT EXISTS `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Dumping data for table deliverit_db.tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;

-- Dumping structure for table deliverit_db.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(45) NOT NULL,
  `roles_id` int(11) DEFAULT 1,
  `createdOn` datetime DEFAULT current_timestamp(),
  `avatar` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `ban_date` datetime DEFAULT NULL,
  `user_city` varchar(45) DEFAULT NULL,
  `user_country` varchar(45) DEFAULT NULL,
  `post_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_roles1_idx` (`roles_id`),
  CONSTRAINT `fk_users_roles1` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table deliverit_db.users: ~11 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `first_name`, `last_name`, `password`, `email`, `roles_id`, `createdOn`, `avatar`, `address`, `ban_date`, `user_city`, `user_country`, `post_code`) VALUES
	(2, 'asen ', 'zhivkov', '$2b$10$Y9o463uPdEo9/DSaXqdv9eb0HJTdKYi2WVWNi97pFED4NtuvIizrC', 'asen@mail.com', 2, '2021-08-12 11:13:42', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'denis', 'mihalev', '$2b$10$WiOu20eUVqhfZaSh94wIgun5Dlwu8p2I89L1qc27FzKFmNJwPIRVG', 'denis@mail.com', 1, '2021-08-12 11:17:30', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'shaquille', 'oatmeal', '$2b$10$MpJ7lc773xAXILqftezf.e3v4icYoJdS5Vw2wLTFCxoB2BF8kHEIe', 'shak@mail.com', 1, '2021-08-25 23:22:01', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'regina', 'phalange', '$2b$10$3eVQIOd/1HZp2bOby14kl.O58ChA0qrDDv2QUE6x14B0GCkFqT.Yu', 'regina@mail.com', 1, '2021-08-25 23:24:58', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'ken ', 'adams', '$2b$10$fi/dPz6OAtLAy6xLfj1RjuTBR4bRKWadakYDqmbmDX93mDMxjJ0r2', 'ken@mail.com', 1, '2021-08-25 23:25:36', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'anony ', 'mouse', '$2b$10$f7aFEIYln/z9CAeg6UrI5.ruBZDIibp88X539tzBkbNudFetePVwy', 'anony@mail.com', 1, '2021-08-25 23:30:55', NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 'gary ', 'snail', '$2b$10$PDksHRPAb71Yq7RxsO.17ufJMIgg8YW2kfWU7X2VrUEWUpBLONgDe', 'gary@mail.com', 1, '2021-08-25 23:34:40', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'Count ', 'Swagula', '$2b$10$PuP4WgU4TLjxB55kiuHxoOP2JNPDoDU7GoJyh009ddmo7MLp9zuBi', 'count@mail.com', 1, '2021-08-25 23:35:32', NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 'bread ', 'pit', '$2b$10$ddgXqISCq8MZN9BPsDI6guQ3ko5CJxIik.RKzl0N3ey.Y36e0824y', 'bread@mail.com', 1, '2021-08-25 23:36:56', NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'LOWERCASE ', 'GUY', '$2b$10$N/X4KkMz03D2Nv422o4ZFO0XL11ucBDhL1eoy2sEbU5IyYgn7svce', 'guy@mail.com', 1, '2021-08-25 23:38:02', NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 'Llama ', 'del Rey', '$2b$10$fUpDzDiOGcL2NqT3jp4tbe7suOlVdMXCgNGd2TiYhxOwnFajd66jq', 'lama@mail.com', 1, '2021-08-25 23:43:49', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table deliverit_db.warehouses
DROP TABLE IF EXISTS `warehouses`;
CREATE TABLE IF NOT EXISTS `warehouses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street_name` varchar(45) NOT NULL,
  `cities_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_warehouses_cities_idx` (`cities_id`),
  CONSTRAINT `fk_warehouses_cities` FOREIGN KEY (`cities_id`) REFERENCES `cities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table deliverit_db.warehouses: ~5 rows (approximately)
/*!40000 ALTER TABLE `warehouses` DISABLE KEYS */;
REPLACE INTO `warehouses` (`id`, `street_name`, `cities_id`) VALUES
	(1, 'Suhodolska 68', 1),
	(2, '110 Downing Str', 7),
	(3, 'W 128th St', 5),
	(4, 'blv. Marica 56', 2),
	(5, 'BundessStrasse 5467', 6);
/*!40000 ALTER TABLE `warehouses` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
